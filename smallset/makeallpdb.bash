#!/bin/bash -x


p=`pwd`
for i in ./Snakefile pdb*/Snakefile */Snakefile
do
    j=`dirname $i`
    cd $j
    for k in `cat zerodockqset.txt` 
    do
	if [[ ${k:0:3} == "pdb" ]]
	then
	    snakemake -p pymodel/${k}_1.pdb
	else
	    snakemake -p pymodel/$k.csv
	fi
    done
    cd $p
done
