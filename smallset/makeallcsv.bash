#!/bin/bash -x

p=`pwd`

for i in ./Snakefile */Snakefile
do
    j=`dirname $i`
    cd $j
    for k in pymodel/*.pdb
    do
	l=`basename $k .pdb`
	snakemake -p pymodel/$l.csv
    done
    cd $p
done
rm results/summary.csv
snakemake -p results/summary.csv
