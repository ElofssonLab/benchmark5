# Dock and Fold

* For publication use benchmark4.3 - the rest are old tests
* To run check Snakefile in each directory
* Scripts to run everything are available at https://github.com/ElofssonLab/bioinfo-toolbox/ (subdir trRosetta/
* benchmark4.3/bin/ is for makeing plots.




* Runs by Gabriele on Kebnekaise see notes.MD for manual analysis of the results

# Status

* AE: Running whole pipeline for all 223 pairs of sequences inheterodimers-pairs.txt on ae18c.

# To do
* Update and clean seq and PDB files from benchmark 5  / Dockground set 4 
  * DONE Use unbound sequences/pdb files
  * DONE/AE (see benchmark4.3/)
  * DONE/GP (see clean_benchmark)
      * selected unbound dimers only from bm5 and dg4 
      * renamed and joined chains 
      * renumbered saved chains in both single and separate files 
      * all files follow a coherent nomenclature (lowcase pdbcodes, ".pdb"->both chains, "_u1.pdb"->receptor, "u2_pdb"->ligand)
  
* Add to snakemake pipeline (see Snakefile_G)
  * DONE/GP pyConfold
  * DONE/GP tmscore scoring
  * DONE/GP qdock scoring
  * DONE/GP AUC scoring
  

* Evaluate distance maps.
  * Different jackhmmer parameters
  * Julies reciprocal best hits
  * Sampling MSA hits from consurf (Gabriele)

* Ways to improve contact maps (docking information)
  * Predict interaction surfaces (look for existing tools - consurf)
  * APC-like correction for "artefacts in contact maps"
  * Evaluate methods for homo-dimers


* Evaluate models
  * TMscore
  * Qdock
  * Agreement in concat maps
  
* Test of ways to make models.
  * pyConfold
  * pyrosetta -allintra
  * Using "real distances" to model each chain
  * Using "single chain distances" to model each chain
  * Errors at the termini
   
  
*  Alternative ways to make models
  * Using "real stucturs" as input to pyrosetta
  * Using Gramm for docking




* Test why some PDB files with many sequence have no inter-chain inforamation
  - Try different alignments
* Test why some dockings seems to have failed although there appears to be some inter-chain information
  - Calculate PPV of contact maps.
