Readme file for datafile for https://doi.org/10.1101/2021.06.04.446442


All scripts for predictions and analysis are available from https://github.com/ElofssonLab/bioinfo-toolbox/trRosetta/
Details for each run are available from https://github.com/ElofssonLab/bioinfo-toolbox/benchmark5/benchmark4.3/.
All models joined alignments, and evaluation results are available from a figshare repository[44].

THe data is organized as follows

1) One diretora (N*/ as well as ./) contains all the results and data for one set of parameters
2) In each directory the following subdirectories are included
2a) seq/  (all sequences)
2b) pdb/ (all orginal pdb files) 
2c) dimer/ all merged msa files
2d) pymodel/ all models generated and the measuremenst (in csv files) to evalute their performance.
3) In the director Figures/ all figures, scripts to generat them as well as summary of all predictions in a csv files is included
