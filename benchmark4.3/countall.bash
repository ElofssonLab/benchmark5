#!/bin/bash 

p=`pwd`
for i in ./Snakefile N1/Snakefile N5/Snakefile N1-N3-merged-new/Snakefile N1-N3-merged/Snakefile N*cov*/Snakefile N*minprob*/Snakefile N*all*/Snakefile N*bact*/Snakefile rbh-jh/Snakefile # confold/Snakefile gramm/Snakefile tmdock-nohomology-double/Snakefile
do
    j=`dirname $i`
    cd $j
    echo $j
    pwd
    echo -n "U1-U2.pdb 05 "
    ls -lrt pymodel/*u1*u2*_A_05.pdb 2>/dev/null |wc -l
    echo -n "U2-U1.pdb 05 "
    ls -lrt pymodel/*u2*u1*_A_05.pdb 2>/dev/null |wc -l 
    echo -n "U1-U2.pdb A  "
    ls -lrt pymodel/*u1*u2*_A.pdb 2>/dev/null |wc -l
    echo -n "U2-U1.pdb A  "
    ls -lrt pymodel/*u2*u1*_A.pdb 2>/dev/null |wc -l 
    echo -n "U1-U2.csv    "
    ls -lrt pymodel/*u1*u2*.csv 2>/dev/null |wc -l
    echo -n "U2-U1.csv    "
    ls -lrt pymodel/*u2*u1*.csv 2>/dev/null |wc -l

    cd $p
done
for i in tmdock-nohomology-double/Snakefile
do
    j=`dirname $i`
    cd $j
    echo $j
    pwd
    echo -n "U1-U2.pdb "
    ls -lrt pymodel/*u1*u2*_A_1.pdb 2>/dev/null |wc -l
    echo -n "U2-U1.pdb "
    ls -lrt pymodel/*u2*u1*_A_1.pdb 2>/dev/null |wc -l 
    echo -n "U1-U2.csv "
    ls -lrt pymodel/*u1*u2*_A_1.csv 2>/dev/null |wc -l
    echo -n "U2-U1.csv "
    ls -lrt pymodel/*u2*u1*_A_1.csv 2>/dev/null |wc -l

    cd $p
done
for i in *confold*/Snakefile
do
    j=`dirname $i`
    cd $j
    echo $j
    pwd
    echo -n "U1-U2.pdb "
    ls -lrt pymodel/*u1*u2*_A_model1.pdb 2>/dev/null |wc -l
    echo -n "U2-U1.pdb "
    ls -lrt pymodel/*u2*u1*_A_model1.pdb 2>/dev/null |wc -l 
    echo -n "U1-U2.csv "
    ls -lrt pymodel/*u1*u2*_A_model1.csv 2>/dev/null |wc -l
    echo -n "U2-U1.csv "
    ls -lrt pymodel/*u2*u1*_A_model1.csv 2>/dev/null |wc -l

    cd $p
done
for i in  gramm*/Snakefile 
do
    j=`dirname $i`
    cd $j
    echo $j
    pwd
    echo -n "U1-U2.pdb "
    ls -lrt pymodel/*u1*u2*_A_*.pdb 2>/dev/null |wc -l
    echo -n "U2-U1.pdb "
    ls -lrt pymodel/*u2*u1*_A_*.pdb 2>/dev/null |wc -l 
    echo -n "U1-U2.csv "
    ls -lrt pymodel/*u1*u2*_A_*.csv 2>/dev/null |wc -l
    echo -n "U2-U1.csv "
    ls -lrt pymodel/*u2*u1*_A_*.csv 2>/dev/null |wc -l

    cd $p
done
#rm results/summary.csv
#snakemake -p results/summary.csv
