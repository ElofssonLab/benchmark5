#!/bin/bash

# To copy PNG figures

#for i in 1vrs 1ay7 3qlu 2o3b 2hrk 2zae 3pv6 4yoc 4gmj
#do
#    rsync -arv ae18c.dyn.scilifelab.se:~/git/benchmark5/benchmark4.3/ ~/git/benchmark5/benchmark4.3/ --exclude "*sto" --exclude "*npz"  --include "*/" --include "*${i}*.png" --exclude "*" --exclude "bin/*"  --exclude "Figures/*" 
#done

# Figure 1

montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -label "(A)" ../N1/results/1vrs_u1_A-1vrs_u2_A.png -label "(B)" 1vrs.png 1vrs-struct.tiff
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -label "(A)" ../N1/results/1vrs_u1_A-1vrs_u2_A.png -label "(B)" 1vrs.png 1vrs-struct.png


montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -label "(B)" ../results/4gmj_u1_A-4gmj_u2_A.png -label "(C)" 4gmj.png 4gmj-struct.tiff
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -label "(B)" ../results/4gmj_u1_A-4gmj_u2_A.png -label "(C)" 4gmj.png 4gmj-struct.png

montage -tile 1x1 -label "(A)" -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x1024+200+0 ../Figures/Fold\ and\ Dock\ Protocol.svg 4gmj-struct.png FoldDock.png
montage -tile 1x1 -label "(A)" -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x1024+200+0 ../Figures/Fold\ and\ Dock\ Protocol.svg 4gmj-struct.png FoldDock.png
montage -tile 1x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x1024+200+0 FoldDock.png 4gmj-struct.png overview.png
montage -tile 1x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x1024+200+0 FoldDock.png  4gmj-struct.png overview.tiff

#cp overview.tiff ../Figures/Figure1.tiff
#cp overview.png ../Figures/Figure1.png

montage -tile 1x1 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x1024+200+0 ../Figures/Fold\ and\ Dock\ Protocol.svg ../Figures/Figure1.eps
montage -tile 1x1 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x1024+200+0 ../Figures/Fold\ and\ Dock\ Protocol.svg ../Figures/Figure1.tiff
montage -tile 1x1 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x1024+200+0 ../Figures/Fold\ and\ Dock\ Protocol.svg ../Figures/Figure1.png




# Figure 2 (performance vd Meff)
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "(A)" Meff-TMscore-Jackhmmer.png  -label "(B)" Meff-dockQ-Jackhmmer.png N3-vs-Meff.tiff
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0   -label "(A)" Meff-TMscore-Jackhmmer.png -label "(B)" Meff-dockQ-Jackhmmer.png N3-vs-Meff.png

cp N3-vs-Meff.tiff ../Figures/Figure2.tiff
cp N3-vs-Meff.png ../Figures/Figure2.png


# Figure 3 (shows that different alignment works sometimes)
montage -tile 2x3 -mode concatenate -density 600  -compress lzw -pointsize 120    -label "(A)" violin-dockQ-first-Rosettaset.png  -label "(B)" good-violin-Rosettaset.png   -label "(C)" ../results/1vrs_u1_A-1vrs_u2_A.png  -label "(D)" ../N1/results/1vrs_u1_A-1vrs_u2_A.png  -label "(E)" ../results/1ay7_u1_A-1ay7_u2_A.png  -label "(F)" ../N1/results/1ay7_u1_A-1ay7_u2_A.png   Comparison-Alignments.tiff  # scatter-Rosettaset-first.png 
montage -tile 2x3 -mode concatenate -density 600  -compress lzw -pointsize 120    -label "(A)" violin-dockQ-first-Rosettaset.png  -label "(B)" good-violin-Rosettaset.png   -label "(C)" ../results/1vrs_u1_A-1vrs_u2_A.png  -label "(D)" ../N1/results/1vrs_u1_A-1vrs_u2_A.png  -label "(E)" ../results/1ay7_u1_A-1ay7_u2_A.png  -label "(F)" ../N1/results/1ay7_u1_A-1ay7_u2_A.png   Comparison-Alignments.png  # scatter-Rosettaset-first.png 



cp Comparison-Alignments.tiff ../Figures/Figure3.tiff
cp Comparison-Alignments.png ../Figures/Figure3.png



convert Comparison-Alignments.tiff -resize 3840x4972+200+0 ../Figures/Figure3-small.tiff
convert Comparison-Alignments.png -resize  3840x4972+200+0 ../Figures/Figure3-small.png


# Figure 3 (Split into 2)
montage -tile 2x1 -mode concatenate -density 600  -compress lzw -pointsize 120    -label "(A)" violin-dockQ-first-Rosettaset.png  -label "(B)"  Comparison-Alignments2.tiff  # scatter-Rosettaset-first.png 
montage -tile 2x1 -mode concatenate -density 600  -compress lzw -pointsize 120    -label "(A)" violin-dockQ-first-Rosettaset.png  -label "(B)" good-violin-Rosettaset.png    Comparison-Alignments2.png  # scatter-Rosettaset-first.png 


# Figure 3 (shows that different alignment works sometimes)
montage -tile 2x2 -mode concatenate -density 600  -compress lzw -pointsize 120     -label "(A)" ../results/1vrs_u1_A-1vrs_u2_A.png  -label "(B)" ../N1/results/1vrs_u1_A-1vrs_u2_A.png  -label "(C)" ../results/1ay7_u1_A-1ay7_u2_A.png  -label "(D)" ../N1/results/1ay7_u1_A-1ay7_u2_A.png   Comparison-Alignments3.tiff  # scatter-Rosettaset-first.png 
montage -tile 2x2 -mode concatenate -density 600  -compress lzw -pointsize 120      -label "(A)" ../results/1vrs_u1_A-1vrs_u2_A.png  -label "(B)" ../N1/results/1vrs_u1_A-1vrs_u2_A.png  -label "(C)" ../results/1ay7_u1_A-1ay7_u2_A.png  -label "(D)" ../N1/results/1ay7_u1_A-1ay7_u2_A.png   Comparison-Alignments3.png  # scatter-Rosettaset-first.png 



cp Comparison-Alignments2.tiff ../Figures/Figure3-new.tiff
cp Comparison-Alignments2.png ../Figures/Figure3-new.png


cp Comparison-Alignments3.tiff ../Figures/Figure3-suppl.tiff
cp Comparison-Alignments3.png ../Figures/Figure3-suppl.png



#convert Comparison-Alignments.tiff -resize 3840x4972+200+0 ../Figures/Figure3-small.tiff
#convert Comparison-Alignments.png -resize  3840x4972+200+0 ../Figures/Figure3-small.png


# Figure x dockingprotocols

montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -label "(A)" violin-dockQ-first-protocolset.png  -label "(B)" violin-TMscore-first-protocolset.png protocols.tiff
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -label "(A)" violin-dockQ-first-protocolset.png  -label "(B)" violin-TMscore-first-protocolset.png protocols.png

cp protocols.tiff ../Figures/Figure4.tiff
cp protocols.png ../Figures/Figure4.png

# Figure 5

montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "(A)" violin-dockQ-first-bigset.png  -label "(B)" violin-TMscore-first-bigset.png violins.tiff
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0   -label "(A)" violin-dockQ-first-bigset.png  -label "(B)" violin-TMscore-first-bigset.png violins.png

#cp performance-violin.tiff ../Figures/Figure4.tiff
#cp performance-violin.png ../Figures/Figure4.png

# Figure 4 - some comparisons

montage -tile 3x1 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "(A)" displot-good-bad-model-log10\ contacts.png  -label "(B)" displot-good-bad-model-log10\ Meff.png  -label "(C)" displot-good-bad-model-taxa.png  performance.tiff
montage -tile 3x1 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "(A)"  displot-good-bad-model-log10\ contacts.png  -label "(B)"  displot-good-bad-model-log10\ Meff.png  -label "(C)" displot-good-bad-model-taxa.png  performance.png

montage -tile 2x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "(A)" jointplot-good-models-bad-model-TMscore-log10\ contacts.png  -label "(B)" violin-dockQ-log10\ contacts-int-Rosettaset.png  -label "(C)" violin-dockQ-log10\ Meff-int-Rosettaset.png  -label "(D)" violin-dockQ-taxa-Rosettaset.png  performance-violin.tiff
montage -tile 2x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "(A)" jointplot-good-models-bad-model-TMscore-log10\ contacts.png  -label "(B)" violin-dockQ-log10\ contacts-int-Rosettaset.png   -label "(C)" violin-dockQ-log10\ Meff-int-Rosettaset.png  -label "(D)" violin-dockQ-taxa-Rosettaset.png  performance-violin.png



#cp performance-violin.tiff ../Figures/Figure5.tiff
#cp performance-violin.png ../Figures/Figure5.png

montage -tile 1x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0   -label "(A)" performance.tiff  -label "(B)" performance-violin.tiff perf.tiff
montage -tile 1x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "(A)"  performance.tiff  -label "(B)" performance-violin.tiff perf.png


montage -tile 3x1 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0   -label "(A)" jointplot-good-models-bad-model-log10\ Meff-TMscore.png  -label "(B)" jointplot-good-models-bad-model-log10\ MCC.png  performance-joint.png
montage -tile 3x1 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0   -label "(A)" jointplot-good-models-bad-model-log10\ Meff-TMscore.png  -label "(B)" jointplot-good-models-bad-model-log10\ MCC.png  performance-joint.tiff

montage -tile 3x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "(A)" displot-good-bad-model-log10\ Meff.png  -label "(B)" displot-good-bad-model-TMscore.png  -label "(C)" displot-good-bad-model-log10\ contacts.png  -label "(D)" displot-good-bad-model-MCC.png  -label "(E)" displot-good-bad-model-Native\ Contacts.png  -label "(F)" displot-good-bad-model-HHprob.png performance-displot.png
montage -tile 3x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "(A)" displot-good-bad-model-log10\ Meff.png  -label "(B)" displot-good-bad-model-TMscore.png  -label "(C)" displot-good-bad-model-log10\ contacts.png  -label "(D)" displot-good-bad-model-MCC.png  -label "(E)" displot-good-bad-model-Native\ Contacts.png  -label "(F)" displot-good-bad-model-HHprob.png performance-displot.tiff


#cp performance-joint.tiff ../Figures/Figure5.tiff
#cp performance-joint.png ../Figures/Figure5.png

cp performance-displot.tiff ../Figures/Figure5.tiff
cp performance-displot.png ../Figures/Figure5.png

#jointplot-good-bad-longF1-log10\ long.png
# Altarnative comparison
#montage -tile 3x1 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 comparison-Meff-Rosettaset-dockQ.png comparison-new.png



# Figure 5 (shows artifacts) # SHould I add a no-contacts also
# perhaps try to


montage -tile 2x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "(A)"  jointplot-good-models-bad-model-log10\ contacts-HHprob.png  -label "(B)" jointplot-good-models-bad-model-log10\ contacts-longF1MODEL.png  -label "(C)" displot-good-bad-model-HHprob.png  -label "(D)" ../results/3qlu_u1_A-3qlu_u2_A.png artifacts.tiff
montage -tile 2x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "(A)"  jointplot-good-models-bad-model-log10\ contacts-HHprob.png  -label "(B)" jointplot-good-models-bad-model-log10\ contacts-longF1MODEL.png -label "(C)"  displot-good-bad-model-HHprob.png  -label "(D)" ../results/3qlu_u1_A-3qlu_u2_A.png artifacts.png

montage -tile 2x1 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0   -label "(A)" displot-good-bad-model-HHprob.png  -label "(B)" ../results/3qlu_u1_A-3qlu_u2_A.png artifacts2.tiff
montage -tile 2x1 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0   -label "(A)" displot-good-bad-model-HHprob.png  -label "(B)" ../results/3qlu_u1_A-3qlu_u2_A.png artifacts2.png


montage -tile 2x3 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "A) 3qlu" ../results/3qlu_u1_A-3qlu_u2_A.png  -label "B) 3qlu (reciprocal)" ../rbh-jh/results/3qlu_u1_A-3qlu_u2_A.png  -label "C) 3pv6"  ../results/3pv6_u1_A-3pv6_u2_A.png  -label "D) 3pv6 (Reciprocal)" ../rbh-jh/results/3pv6_u1_A-3pv6_u2_A.png  -label "E) 4yoc " ../results/4yoc_u1_A-4yoc_u2_A.png  -label "F) 4yoc (reciprocal)"  ../rbh-jh/results/4yoc_u1_A-4yoc_u2_A.png artifacts3.png
montage -tile 2x3 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "A) 3qlu" ../results/3qlu_u1_A-3qlu_u2_A.png  -label "B) 3qlu (reciprocal)" ../rbh-jh/results/3qlu_u1_A-3qlu_u2_A.png  -label "C) 3pv6"  ../results/3pv6_u1_A-3pv6_u2_A.png  -label "D) 3pv6 (Reciprocal)" ../rbh-jh/results/3pv6_u1_A-3pv6_u2_A.png  -label "E) 4yoc " ../results/4yoc_u1_A-4yoc_u2_A.png  -label "F) 4yoc (reciprocal)"  ../rbh-jh/results/4yoc_u1_A-4yoc_u2_A.png artifacts3.tiff

montage -tile 3x1 -mode concatenate -density 600  -compress lzw -pointsize 90 -geometry 2048x2048+10+10  -label "3qlu" ../results/3qlu_u1_A-3qlu_u2_A.png  -label "3pv6"   ../results/3pv6_u1_A-3pv6_u2_A.png  -label "4yoc" ../results/4yoc_u1_A-4yoc_u2_A.png  artifacts4.png
montage -tile 3x1 -mode concatenate -density 600  -compress lzw -pointsize 90 -geometry 2048x2048+10+10  -label "3qlu" ../results/3qlu_u1_A-3qlu_u2_A.png  -label "3pv6"  ../results/3pv6_u1_A-3pv6_u2_A.png  -label "4yoc" ../results/4yoc_u1_A-4yoc_u2_A.png  artifacts4.tiff
montage -tile 3x1 -mode concatenate -density 600  -compress lzw -pointsize 90 -geometry 2048x2048+10+10  ../results/3qlu_u1_A-3qlu_u2_A.png   ../results/3pv6_u1_A-3pv6_u2_A.png  ../results/4yoc_u1_A-4yoc_u2_A.png  artifacts4.png
montage -tile 3x1 -mode concatenate -density 600  -compress lzw -pointsize 90 -geometry 2048x2048+10+10  ../results/3qlu_u1_A-3qlu_u2_A.png   ../results/3pv6_u1_A-3pv6_u2_A.png   ../results/4yoc_u1_A-4yoc_u2_A.png  artifacts4.tiff

montage -tile 3x1 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "A) 3qlu (reciprocal)" ../rbh-jh/results/3qlu_u1_A-3qlu_u2_A.png  -label "B) 3pv6 (reciprocal)"  ../rbh-jh/results/3pv6_u1_A-3pv6_u2_A.png  -label "C) 4yoc (reciprocal)" ../rbh-jh/results/4yoc_u1_A-4yoc_u2_A.png  artifacts5.png
montage -tile 3x1 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "A) 3qlu (reciprocal)" ../rbh-jh/results/3qlu_u1_A-3qlu_u2_A.png  -label "B) 3pv6 (reciprocal)"  ../rbh-jh/results/3pv6_u1_A-3pv6_u2_A.png  -label "C) 4yoc (reciprocal)" ../rbh-jh/results/4yoc_u1_A-4yoc_u2_A.png  artifacts5.tiff




cp artifacts3.tiff ../Figures/Figure6.tiff
cp artifacts3.png ../Figures/Figure6.png

cp artifacts4.tiff ../Figures/Figure6-new.tiff
cp artifacts4.png ../Figures/Figure6-new.png

cp artifacts5.tiff ../Figures/Figure6-suppl.tiff
cp artifacts5.png ../Figures/Figure6-suppl.png


# Figure 6  Pconsdock and mmdock

montage -tile 2x2 -mode concatenate -density 600  -compress lzw -pointsize 120   -label "(A)" jointplot-good-models-bad-model-PconsDock-MMdock.png  -label "(B)" jointplot-good-models-bad-model-pcd-mmd.png  -label "(C)" ROC-pconsdockRosettaset.png  -label "(E)" violin-dockQ-first-PconsDockset.png PconsDock.tiff
montage -tile 2x2 -mode concatenate -density 600  -compress lzw -pointsize 120  -label "(A)"  jointplot-good-models-bad-model-PconsDock-MMdock.png  -label "(B)" jointplot-good-models-bad-model-pcd-mmd.png  -label "(C)" ROC-pconsdockRosettaset.png  -label "(D)" violin-dockQ-first-PconsDockset.png PconsDock.png
montage -tile 2x2 -mode concatenate -density 600  -compress lzw -pointsize 120  -label "(A)" violin-dockQ-first-PconsDockset.png  -label "(B)"  ROC-pconsdockRosettaset.png  PconsDock-ROC.tiff
montage -tile 2x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -label "(A)" violin-dockQ-first-PconsDockset.png  -label "(B)" ROC-pconsdockRosettaset.png PconsDock-ROC.png

cp PconsDock-ROC.png ../Figures/FigureS4.png
cp PconsDock-ROC.tiff ../Figures/FigureS4.tiff

# figure 9 Comparison with gramm and tmdock
montage -tile 2x1 -mode concatenate -density 600  -compress lzw -pointsize 120    -label "(A)" violin-dockQ-first-dockingset.png  -label "(B)" scatter-dockingset-first.png   Comparison-docking.tiff
montage -tile 2x1 -mode concatenate -density 600  -compress lzw -pointsize 120   -label "(A)" violin-dockQ-first-dockingset.png  -label "(B)" scatter-dockingset-first.png   Comparison-docking.png

montage -tile 2x2 -mode concatenate -density 600  -compress lzw -pointsize 120   -label "(A)"  violin-dockQ-first-dockingset.png  -label "(B)" scatter-dockingset-first.png  -label "(C)"  violin-dockQ-first-grammset.png  -label "(D)" scatter-grammset-first.png  Comparison-docking-all.tiff
montage -tile 2x2 -mode concatenate -density 600  -compress lzw -pointsize 120  -label "(A)"   violin-dockQ-first-dockingset.png  -label "(B)" scatter-dockingset-first.png  -label "(C)"  violin-dockQ-first-grammset.png  -label "(D)" scatter-grammset-first.png  Comparison-docking-all.png


cp Comparison-docking.png ../Figures/Figure8.png
cp Comparison-docking.tiff ../Figures/Figure8.tiff


# Different datasets

montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "(A)"  violin-dockQ-first-taxaset.png  -label "(B)" scatter-taxaset-first.png taxas.tiff
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0   -label "(A)" violin-dockQ-first-taxaset.png  -label "(B)" scatter-taxaset-first.png taxas.png


montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "(A)"  ../results/2o3b_u1_A-2o3b_u2_A.png  -label "(B)" ../N3-bacterial/results/2o3b_u1_A-2o3b_u2_A.png  2o3b-maps.tiff
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0   -label "(A)" ../results/2o3b_u1_A-2o3b_u2_A.png  -label "(B)" ../N3-bacterial/results/2o3b_u1_A-2o3b_u2_A.png  2o3b-maps.png

montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0  -label "(A)" ../N1/results/2hrk_u2_A-2hrk_u1_A.png  -label "(B)" ../N1b-allproteomes/results/2hrk_u2_A-2hrk_u1_A.png  2hrk-maps.tiff
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0   -label "(A)" ../N1/results/2hrk_u2_A-2hrk_u1_A.png  -label "(B)" ../N1b-allproteomes/results/2hrk_u2_A-2hrk_u1_A.png  2hrk-maps.png

montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0   -label "(A)" ../results/2zae_u2_A-2zae_u1_A.png  -label "(B)" ../N3-allproteomes/results/2zae_u2_A-2zae_u1_A.png  2zae-maps.tiff
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0   -label "(A)" ../results/2zae_u2_A-2zae_u1_A.png  -label "(B)" ../N3-allproteomes/results/2zae_u2_A-2zae_u1_A.png  2zae-maps.png

montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0   -label "(A)" ../N3-allproteomes/results/2zae_u2_A-2zae_u1_A.png  -label "(B)" 2zae.png 2zae-struct.tiff
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0   -label "(A)" ../N3-allproteomes/results/2zae_u2_A-2zae_u1_A.png  -label "(B)" 2zae.png 2zae-struct.png

montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -tile 2x3  -label "(A)" ../results/2o3b_u1_A-2o3b_u2_A.png  -label "(B)" ../N3-bacterial/results/2o3b_u1_A-2o3b_u2_A.png  -label "(C)"  ../N1/results/2hrk_u2_A-2hrk_u1_A.png -label "(D)" ../N1b-allproteomes/results/2hrk_u2_A-2hrk_u1_A.png  -label "(E)" ../N3-allproteomes/results/2zae_u2_A-2zae_u1_A.png  -label "(F)" 2zae.png extra-proteomes.png
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -tile 2x3  -label "(A)"  ../results/2o3b_u1_A-2o3b_u2_A.png  -label "(B)" ../N3-bacterial/results/2o3b_u1_A-2o3b_u2_A.png  -label "(C)"  ../N1/results/2hrk_u2_A-2hrk_u1_A.png  -label "(D)" ../N1b-allproteomes/results/2hrk_u2_A-2hrk_u1_A.png  -label "(E)" ../N3-allproteomes/results/2zae_u2_A-2zae_u1_A.png  -label "(F)" 2zae.png extra-proteomes.tiff

montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -tile 2x3  -label "(A)"  violin-dockQ-taxa-Rosettaset.png  -label "(B)" violin-TMscore-taxa-Rosettaset.png  -label "(C)" violin-dockQ-first-taxaset.png  -label "(D)" violin-TMscore-first-taxaset.png  -label "(E)" scatter-taxaset-first.png  -label "(F)" ../N3-allproteomes/results/2zae_u2_A-2zae_u1_A.png taxaset.tiff
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -tile 2x3  -label "(A)"  violin-dockQ-taxa-Rosettaset.png  -label "(B)" violin-TMscore-taxa-Rosettaset.png  -label "(C)" violin-dockQ-first-taxaset.png  -label "(D)" violin-TMscore-first-taxaset.png  -label "(E)" scatter-taxaset-first.png  -label "(F)" ../N3-allproteomes/results/2zae_u2_A-2zae_u1_A.png taxaset.png


montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -tile 3x1   -label "(A)" violin-dockQ-taxa-Rosettaset.png  -label "(B)"  violin-dockQ-first-taxaset.png  -label "(C)" scatter-taxaset-first.png  taxaset-small.tiff
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -tile 3x1  -label "(A)"  violin-dockQ-taxa-Rosettaset.png  -label "(B)"  violin-dockQ-first-taxaset.png  -label "(C)" scatter-taxaset-first.png  taxaset-small.png


cp taxaset-small.tiff ../Figures/Figure7.tiff
cp taxaset-small.png ../Figures/Figure7.png

#cp 2zae-struct.tiff ../Figures/Figure8.tiff
#cp 2zae-struct.png ../Figures/Figure8.png

# Figures for paper.

# New merge of Fig7+8+9



montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -tile 3x1   -label "(A)" violin-dockQ-taxa-Rosettaset.png  -label "(B)"  violin-dockQ-first-taxaset.png  -label "(C)" scatter-taxaset-first.png  taxaset-small.tiff
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -tile 3x1  -label "(A)"  violin-dockQ-taxa-Rosettaset.png  -label "(B)"  violin-dockQ-first-taxaset.png  -label "(C)" scatter-taxaset-first.png  taxaset-small.png
montage -tile 2x1 -mode concatenate -density 600  -compress lzw -pointsize 120    -label "(A)" violin-dockQ-first-dockingset.png  -label "(B)" scatter-dockingset-first.png   Comparison-docking.tiff
montage -tile 2x1 -mode concatenate -density 600  -compress lzw -pointsize 120   -label "(A)" violin-dockQ-first-dockingset.png  -label "(B)" scatter-dockingset-first.png   Comparison-docking.png


# H1065 figure..

montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -tile 2x1   -label "(A)" ../Figures/T1065a-T1065b.png  -label "(B)" ../Figures/H1065.png ../Figures/FigureS5.png
montage -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -tile 2x1  -label "(A)"  ../Figures/T1065a-T1065b.png  -label "(B)" ../Figures/H1065.png  ../Figures/FigureS5.tiff


