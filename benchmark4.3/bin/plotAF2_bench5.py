import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import re
import pandas as pd
from matplotlib.patches import Rectangle

sns.set(style="whitegrid")
sns.set(rc={'figure.figsize':(12,12)})


df_bench5=pd.read_csv("~/Downloads/bench5.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1)
df_bench5_gramm=pd.read_csv("~/Downloads/benchmark5_dockq_gramm.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1,index_col=False)

df_bench5_gramm=df_bench5_gramm.rename(columns={  "complex_id":"PDB"})
df_bench5=df_bench5.rename(columns={  "DockQ_dockqstats_bench5":"DockQ"})


df={"AF2":df_bench5,"GRAMM":df_bench5_gramm}

list={"AF2","GRAMM"}
for i in list:
    for j in list:
        print (i,j)
        #print (df[i]["rank"])
        #print (df[j]["rank"])
        df_merged=pd.merge(df[i],df[j],on=["PDB"])
        sns.set(style="whitegrid")
        f, ax = plt.subplots(figsize=(9, 9))
        sns.set(style="whitegrid")
        sns.set(rc={'figure.figsize':(9,9)})
        palette = sns.color_palette("tab10", 10)
        df_merged["dx"]=df_merged.DockQ_x.gt(0.23).astype(int)
        df_merged["dy"]=df_merged.DockQ_y.gt(0.23).astype(int)
        df_merged["correct"]=(df_merged.dx)+(df_merged.dy)
        #df_merged["correct"]=df_merged[["DockQ_x","DockQ_y"]].max(axis=1)
        #df_merged["correct"]=(df_merged.DockQ_x)-(df_merged.DockQ_y)
        sns_plot=sns.scatterplot(df_merged.DockQ_x,df_merged.DockQ_y,hue=df_merged.correct,legend=False)
        f=sns_plot.get_figure()
        sns_plot.set_title("Preotein-Protein comparison " + i + " vs " + j)
        averageX=np.round(np.average(df_merged.DockQ_x),2)
        averageY=np.round(np.average(df_merged.DockQ_y),2)
        numX=np.sum(df_merged.DockQ_x>0.23)
        numY=np.sum(df_merged.DockQ_y>0.23)
        length=len(df_merged.DockQ_y)
        sns_plot.set_xlabel("DockQ "+i+" Average DockQ=" + str(averageX) +", FracCorrect="+ str(round(100*numX/length,0)) +"% ")
        sns_plot.set_ylabel("DockQ "+j+" Average DockQ=" + str(averageY) +", FracCorrect="+ str(round(100*numY/length,0)) +"% ")
        sns_plot.set(xlim=[0,1])
        sns_plot.set(ylim=[0,1])
        x=[0,1]
        palette = sns.color_palette("tab10", 6)
        sns_plot=sns.lineplot(x,x,palette=palette)
        d1=[0,0.23,0.23]
        d2=[0.23,0.23,0]
        #sns_plot=sns.lineplot(d1,d2,palette=palette)
        plt.plot(d1, d2, linewidth=2)
        #sns_plot=sns.lineplot(d2,d1,palette=palette)
        currentAxis = plt.gca()
        currentAxis.add_patch(Rectangle((0, 0), 0.23, 0.23,alpha=0.4))
        f.savefig("Scatter-"+i+"-"+j+".svg")
        plt.close(fig="all")
