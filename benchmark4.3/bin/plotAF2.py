import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import re
import pandas as pd
from matplotlib.patches import Rectangle

sns.set(style="whitegrid")
sns.set(rc={'figure.figsize':(12,12)})


dfAFm=pd.read_csv("../AF2-merged/results/summary.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1,index_col=False)
dfAFb=pd.read_csv("../AF2-smallbfd/results/summary.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1,index_col=False)
dfAFp=pd.read_csv("../AF2-proteomes/results/summary.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1,index_col=False)
dfgramm=pd.read_csv("../gramm/results/summary.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1,index_col=False)
dftmdock=pd.read_csv("../tmdock/results/summary.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1,index_col=False)
dfN3=pd.read_csv("../results/summary2.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1,index_col=False)
dfRFoldE2E=pd.read_csv("../N3-RoseTTAFold-E2E/results/summary.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1,index_col=False)
dfRFoldpyR=pd.read_csv("../N3-RoseTTAFold/results/summary.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1,index_col=False)
dfRFoldE2E2=pd.read_csv("../RoseTTAFold-default-merged/results/summary.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1,index_col=False)
dfRFoldpyR2=pd.read_csv("../RoseTTAFold-merged-pyRosetta/results/summary.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1,index_col=False)

df={"AF2-merged":dfAFm,"AF2-BFD":dfAFb,"AF2-proteomes":dfAFp,"gramm":dfgramm,"TMdock":dftmdock,"N3":dfN3,"RoseTTAFold-E2E":dfRFoldE2E,"RoseTTAFold-pyR":dfRFoldpyR,"RoseTTAFold-E2E2":dfRFoldE2E2,"RoseTTAFold-pyR2":dfRFoldpyR2}
list={"AF2-merged","AF2-BFD","AF2-proteomes","gramm","TMdock","N3","RoseTTAFold-E2E","RoseTTAFold-pyR","RoseTTAFold-E2E2","RoseTTAFold-pyR2"}
for i in list:
    for j in list:
        print (i,j)
        #print (df[i]["rank"])
        #print (df[j]["rank"])
        df_merged=pd.merge(df[i].loc[df[i]["rank"]==1],df[j].loc[df[j]["rank"]==1],on=["name"])
        sns.set(style="whitegrid")
        f, ax = plt.subplots(figsize=(9, 9))
        sns.set(style="whitegrid")
        sns.set(rc={'figure.figsize':(9,9)})
        palette = sns.color_palette("tab10", 10)
        df_merged["dx"]=df_merged.dockQ_x.gt(0.23).astype(int)
        df_merged["dy"]=df_merged.dockQ_y.gt(0.23).astype(int)
        df_merged["correct"]=(df_merged.dx)+(df_merged.dy)
        #df_merged["correct"]=df_merged[["dockQ_x","dockQ_y"]].max(axis=1)
        #df_merged["correct"]=(df_merged.dockQ_x)-(df_merged.dockQ_y)
        sns_plot=sns.scatterplot(df_merged.dockQ_x,df_merged.dockQ_y,hue=df_merged.correct,legend=False)
        f=sns_plot.get_figure()
        sns_plot.set_title("Preotein-Protein comparison " + i + " vs " + j)
        averageX=np.round(np.average(df_merged.dockQ_x),2)
        averageY=np.round(np.average(df_merged.dockQ_y),2)
        numX=np.sum(df_merged.dockQ_x>0.23)
        numY=np.sum(df_merged.dockQ_y>0.23)
        length=len(df_merged.dockQ_y)
        sns_plot.set_xlabel("dockQ "+i+" Average DockQ=" + str(averageX) +", FracCorrect="+ str(round(100*numX/length,0)) +"% ")
        sns_plot.set_ylabel("dockQ "+j+" Average DockQ=" + str(averageY) +", FracCorrect="+ str(round(100*numY/length,0)) +"% ")
        sns_plot.set(xlim=[0,1])
        sns_plot.set(ylim=[0,1])
        x=[0,1]
        palette = sns.color_palette("tab10", 6)
        sns_plot=sns.lineplot(x,x,palette=palette)
        d1=[0,0.23,0.23]
        d2=[0.23,0.23,0]
        #sns_plot=sns.lineplot(d1,d2,palette=palette)
        plt.plot(d1, d2, linewidth=2)
        #sns_plot=sns.lineplot(d2,d1,palette=palette)
        currentAxis = plt.gca()
        currentAxis.add_patch(Rectangle((0, 0), 0.23, 0.23,alpha=0.4))
        f.savefig("Scatter-"+i+"-"+j+".svg")
        plt.close(fig="all")
