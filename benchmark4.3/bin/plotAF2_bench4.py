import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import re
import pandas as pd
from matplotlib.patches import Rectangle

sns.set(style="whitegrid")
sns.set(rc={'figure.figsize':(12,12)})
sns.set(font_scale=1.3)
dfgramm=pd.read_csv("../gramm/results/summary.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1,index_col=False)
dfAFb=pd.read_csv("../AF2-smallbfd/results/summary.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1,index_col=False)
df_bench4=pd.read_csv("~/Downloads/bench4_model2_dockqstats.csv",sep=",",warn_bad_lines=True,error_bad_lines=True,na_values=-1)

dfgramm["code"]=dfgramm["name"].str[:4]
dfAFb["code"]=dfAFb["name"].str[:4]
df_bench4["code"]=df_bench4["complex_id"].str[:4]
df_bench4["rank"]=1
df_bench4=df_bench4.rename(columns={"DockQ_dockqstats_bench4_af2_af2stdmsa_model_2_ens8":"DockQ"})
dfgramm=dfgramm.rename(columns={"dockQ":"DockQ"})
dfAFb=dfAFb.rename(columns={"dockQ":"DockQ"})


df={"AF2_model1":dfAFb,"AlphaFold2":df_bench4,"GRAMM":dfgramm}



list={"AF2_model1","AlphaFold2","GRAMM"}
for i in list:
    for j in list:
        if i>=j: next
        print (i,j)
        #print (df[i]["rank"])
        #print (df[j]["rank"])
        df_merged=pd.merge(df[i].loc[df[i]["rank"]==1],df[j].loc[df[j]["rank"]==1],on=["code"])
        sns.set(style="whitegrid")
        sns.set(font_scale=1.3)
        f, ax = plt.subplots(figsize=(9, 9))
        sns.set(style="whitegrid")
        sns.set(rc={'figure.figsize':(9,9)})
        palette = sns.color_palette("tab10", 10)
        df_merged["dx"]=df_merged.DockQ_x.gt(0.23).astype(int)
        df_merged["dy"]=df_merged.DockQ_y.gt(0.23).astype(int)
        df_merged["correct"]=(df_merged.dx)+(df_merged.dy)
        #df_merged["correct"]=df_merged[["DockQ_x","DockQ_y"]].max(axis=1)
        #df_merged["correct"]=(df_merged.DockQ_x)-(df_merged.DockQ_y)
        sns_plot=sns.scatterplot(df_merged.DockQ_x,df_merged.DockQ_y,hue=df_merged.correct,legend=False,s=200)
        f=sns_plot.get_figure()
        sns_plot.set_title("DockQ " + i + " vs " + j,fontsize=25)
        averageX=np.round(np.average(df_merged.DockQ_x),2)
        averageY=np.round(np.average(df_merged.DockQ_y),2)
        numX=np.sum(df_merged.DockQ_x>0.23)
        numY=np.sum(df_merged.DockQ_y>0.23)
        length=len(df_merged.DockQ_y)
        sns_plot.set_xlabel("DockQ "+i+" Average DockQ=" + str(averageX) +", FracCorrect="+ str(round(100*numX/length,0)) +"% ")
        sns_plot.set_ylabel("DockQ "+j+" Average DockQ=" + str(averageY) +", FracCorrect="+ str(round(100*numY/length,0)) +"% ")
        sns_plot.set(xlim=[0,1])
        sns_plot.set(ylim=[0,1])
        x=[0,1]
        palette = sns.color_palette("tab10", 6)
        sns_plot=sns.lineplot(x,x,palette=palette)
        d1=[0,0.23,0.23]
        d2=[0.23,0.23,0]
        #sns_plot=sns.lineplot(d1,d2,palette=palette)
        plt.plot(d1, d2, linewidth=2)
        #sns_plot=sns.lineplot(d2,d1,palette=palette)
        currentAxis = plt.gca()
        currentAxis.add_patch(Rectangle((0, 0), 0.23, 0.23,alpha=0.4))
        f.savefig("Scatter-"+i+"-"+j+".svg")
        plt.close(fig="all")
