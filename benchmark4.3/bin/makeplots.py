#!/usr/bin/env python3

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import re
import pandas as pd
import sklearn.metrics as metrics


import sklearn
import sklearn.metrics as metrics
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline


sns.set(font_scale=1.4,style="whitegrid",rc={'figure.figsize':(12,12)})
# Main code

dtype_dic= {"name":str,"JHparams":str,"lenA":int,"lenB":int,"msa":int,"msaA":int,"msaB":int,"cdhit":int,"dockQ":float,"fnat":float,"iRMS":float,"LRMS":float,"fnonnat":float,"mm":float,"tmA":float,"tmB":float,"long":int,"med":int,"longPPV":float,"medPPV":float,"longA":int,"medA":int,"longPPVA":float,"medPPVA":float,"longB":int,"medB":int,"longPPVB":float,"medPPVB":float,"short":int,"shortPPV":float,"shortA":int,"shortPPVA":float,"shortB":int,"shortPPVB":float,"shortpdb":int,"medpdb":int,"longpdb":int,"avprob":float,"longMODEL":int,"medMODEL":int,"longPPVMODEL":float,"medPPVMODEL":float,"longAMODEL":int,"medAMODEL":int,"longPPVAMODEL":float,"medPPVAMODEL":float,"longBMODEL":int,"medBMODEL":int,"longPPVBMODEL":float,"medPPVBMODEL":float,"shortMODEL":int,"shortPPVMODEL":float,"shortAMODEL":int,"shortPPVAMODEL":float,"shortBMODEL":int,"shortPPVBMODEL":float,"shortpdbMODEL":float,"medpdbMODEL":float,"longpdbMODEL":float,"avprobMODEL":float,"evalue":float,"seqid":float,"hhprob":float,"hhevalue":float,"hhscore":float,"PconsDock":float,"MMdock":float,"shortMCC":float,"medMCC":float,"longMCC":float,"shortF1":float,"medF1":float,"longF1":float,"shortMCCMODEL":float,"medMCCMODEL":float,"longMCCMODEL":float,"shortF1MODEL":float,"medF1MODEL":float,"longF1MODEL":float,"rank":int,"pcd":float,"mmd":float ,"taxa":int}

#try:
df=pd.read_csv("~/Downloads/summary.csv",sep=",",dtype=dtype_dic,warn_bad_lines=True,error_bad_lines=True,na_values=-1,index_col=False)


#except:
#    df=pd.read_csv("~/Downloads/summary.csv",sep=",",na_values=-1)
#    for d in df.keys():
#        #print (d,is_numeric_dtype(df[d]))
#        is_error = pd.to_numeric(df[d], errors='coerce').isna()
#        if (len(is_error)>1): 
#            print(d,df[is_error])
#    sys.exit()

print ("Original len:", len(df))
df=df.replace(np.nan, -1.)
#df=df.dropna(axis=0, how="any")
#print (df.loc[df.JHparams=="N3-pdb"])
df=df.dropna()
print ("After skipping:", len(df))

#print (df.loc[df.JHparams=="N3-pdb"])
#print (df.taxa)
#sys.exit()

df["code"]=df["name"].str[:4]
df["CODE"]=df["code"]
df["params"]=df["JHparams"]
datasets=df.JHparams.unique()

df["TMscore"]=df[["tmA","tmB"]].mean(axis=1)
df["MSA"]=df[["msaA","msaB"]].mean(axis=1)
df["TMscore_max"]=df[["tmA","tmB"]].max(axis=1)
df["MSA_max"]=df[["msaA","msaB"]].max(axis=1)
df["TMscore_min"]=df[["tmA","tmB"]].min(axis=1)
df["MSA_min"]=df[["msaA","msaB"]].min(axis=1)
df["numcorrect"]=df["longPPV"]*df["long"]
df["len"]=df["lenA"]+df["lenB"]
df["TPR"]=df.numcorrect/df.longpdb


# Some renaming
df=df.replace({
        '-N 1 -E 1.e-10 --cpu 8 ':'N1',
        '-N 1 -E 1.e-2':'N1b',
        '-N 3 --cpu 8':'N3',
        '-N 5 --cpu 8':'N5',
        'AF2-smallbsd':'AF2-smallBFD',
        'AF2-smallbfd':'AF2-smallBFD',
        'confold':'pyconsFold',
        'confold-pdb':'pyconsFold-pdb',
        'confold-merged':'pyconsFold-merged',
        'confold-contacts':'pyconsFold-contacts',
        'gramm':'GRAMM',
        'gramm-contact':'GRAMM-contact',
        'gramm-raptorx':'GRAMM-raptorx',
        'gramm-trmodels':'GRAMM-trmodels',
        'gramm-score':'GRAMM-score',
        })

df=df.rename(columns={
        "cdhit":"Meff",
        "MMdock":"MMpair",
        "mmd":"MMcons",
        "PconsDock":"dockQpair",
        "pcd":"dockQcons",
        "long":"contacts",
        "longPPV":"PPV",
        "longMCC":"MCC",
        "longF1":"F1",
        "longpdb":"Native Contacts",
        "hhprob":"HHprob"
})




# Define some sets
protocolset=['N3',"pyconsFold","RaptorX","N3-merged","N3-pdb"]
oneset=['N3']
smallset=['N1','N3', 'N5']
taxaset=["N1","N1-all",'N3','N3-bact',"N3-all"]
PconsDockset=["N3",'PconsDock-dockQ','PconsDock-dockQcons','PconsDock-MMcons','PconsDock-dockQpair','PconsDock-MMpair']
Rosettaset=['N1','N3', 'N1-cov90','N3-cov90',"N1-minprob25","N3-minprob25"] 
AF2set=["AF2-merged","AF2-smallBFD","AF2-proteomes"]
PconsDockset2=Rosettaset+['PconsDock-dockQ','PconsDock-dockQcons']
RosettasetA=['N1', 'N3-cov90',"N1-minprob25"] 
RosettasetB=['N3', 'N1-cov90',"N3-minprob25"] 
#Rosettaset=['N1-cov90','N3-cov90',"N1-minprob25","N3-minprob25"] 
#RosettasetA=[ 'N3-cov90'] 
#RosettasetB=[ 'N1-cov90'] 
bigset=['N1','N3','N5', 'N1-cov90','N3-cov90', 'N1-cov50','N3-cov50',"N1-minprob25","N3-minprob25","N3-pdb","N3-merged","pyconsFold","rbh","rbh-jh"] # ,"PconsDock","pyconsFold"] 
comboset=Rosettaset+['N1-N3-multimerged', 'N1-N3-new', 'N1-N3-cov90', 'N1-N3-cov-nocov'] +["PconsDock"] 
dockingset=["GRAMM","GRAMM-contact","GRAMM-raptorx","TMdock-double","PconsDock"]
#dockingset=["GRAMM","TMdock-double","PconsDock","AF2-merged","AF2-smallBFD","AF2-proteomes"]
#dockingset=["GRAMM","GRAMM-contact","GRAMM-raptorx","TMdock-double","PconsDock","AF2-merged"]
grammset=["GRAMM","GRAMM-contact","GRAMM-trmodels","GRAMM-raptorx","GRAMM-score"] # ,"GRAMM-score2"]
pyconsFoldset=["pyconsFold","pyconsFold-pdb","pyconsFold-contacts","N3"] # "pyconsFold-npz", ,"pyconsFold-merged" ,"pdbpyconsFold"
allset=list(set(Rosettaset+comboset+dockingset+grammset+AF2set))

# Comparison of good vs bad models.
df["log10 Meff"]=df.Meff.apply(lambda x:(np.log10(x+0.1)))
df["log10 Meff-int"]=df["log10 Meff"].astype(int)
df["log10 avprob"]=df.avprob.apply(lambda x:(np.log10(x+0.1)))
df["log10 HHprob"]=df.HHprob.apply(lambda x:(np.log10(x+0.1)))
df["log10 contacts"]=df.contacts.apply(lambda x:(np.log10(x+0.1)))
df["log10 contacts-int"]=df["log10 contacts"].astype(int)
df["log10 med"]=df.med.apply(lambda x:(np.log10(x+0.1)))
df["log10 med-int"]=df["log10 med"].astype(int)
df["log10 short"]=df.short.apply(lambda x:(np.log10(x+0.1)))
df["log10 short-int"]=df["log10 short"].astype(int)
df["log10 Native Contacts"]=df["Native Contacts"].apply(lambda x:(np.log10(x+0.1)))
df["log10 Native Contacts-int"]=df["log10 Native Contacts"].astype(int)
df["log10 medpdb"]=df.medpdb.apply(lambda x:(np.log10(x+0.1)))
df["log10 medpdb-int"]=df["log10 medpdb"].astype(int)
df["log10 shortpdb"]=df.shortpdb.apply(lambda x:(np.log10(x+0.1)))
df["log10 shortpdb-int"]=df["log10 shortpdb"].astype(int)
df["log10 hhevalue"]=df.hhevalue.apply(lambda x:(np.log10(x+0.1)))
#keys=[ 'Meff',"msa", 'contacts',"med", 'shortpdb', 'HHprob',"seqid","log10 hhevalue",'dockQpair', 'MMpair', 'MCC', 'F1', 'medMCC', 'medF1', 'shortMCC', 'shortF1', 'TMscore', 'len',"log10 Meff","log10 contacts","dockQcons","MMcons","shortpdb","medpdb","Native Contacts","avprob","log10 avprob","log10 HHprob","shortMODEL","medMODEL","longMODEL","shortPPVMODEL","medPPVMODEL","longPPVMODEL"] # 
skipkeys=set(["name","params","JHparams","code","CODE"])

skipkeys2=set(["avprob","avprobMODEL","evalue","hhevalue","lenA","lenB","len","log10 avprob","log10 hhevalue","longAMODEL","longBMODEL","longA","longB","longpdbMODEL","longMODEL","longPPVAMODEL","longPPVBMODEL","medAMODEL","medBMODEL","medpdbMODEL","medPPVAMODEL","medPPVBMODEL","MSA_max","MSA_min","MSA","msaA","msa","msaB","rank","seqid","shortAMODEL","shortMODEL","shortBMODEL","shortA","shortB","shortF1MODEL","shortMCCMODEL","shortMODEL","shortpdbMODEL","shortPPVAMODEL","shortPPVMODEL","shortPPVBMODEL","short","TMscore_max","TMscore_min","tmA","tmB"])  # No information

skipkeys3=set(["fnat","fnonnat","iRMS","longPPVA","longPPVB","LRMS","medA","med","medB","short","shortA","shortB","medF1","medPPVA","medMCC","medPPV","medPPVB","shortF1","shortMCC","shortPPVA","shortPPVB","shortPPV"]) # Actually decribes the structure (kept mm and dockQ) # ,"F1"
allkeys=set(df.keys())
mykeys=allkeys-skipkeys-skipkeys2-skipkeys3
print (mykeys)
#sys.exit()
#selset=["PconsDock"]
sets2={"Rosettaset":Rosettaset,"AF2set":AF2set} #,"N-test":["N1","N3"] # ,"N5" "PconsDock":["PconsDock"],
mets=[ 'dockQpair', 'dockQcons','MMpair','MMcons', 'Meff', 'contacts', 'TMscore', 'longPPVMODEL','log10 avprob','HHprob','log10 Meff','log10 HHprob']
modset=["dockQpair", "MMpair","dockQcons","MMcons"]
predset=["log10 avprob","contacts"]
seqset=["log10 Meff","HHprob"]
tmset=["TMscore","longPPVMODEL"]
cheatset=["F1","MCC","shortF1","medF1"]
methodsets={"modelinfo":modset,
            "seqinfo":seqset,
            "structinfo":tmset,
            "predinfo":predset,
            "cheatinfo":cheatset,
            "m+s-info":modset+seqset,
            "m+t-info":modset+seqset,
            "m+s+t-info":modset+seqset+tmset,
            "m+s+p-info":modset+seqset+predset,
            "m+s+p+t-info":modset+seqset+predset+tmset,
            "s+p-info":seqset+predset,
            "m+p-info":modset+seqset,
            "s+p+t-info":seqset+predset+tmset,
            "m+p+t-info":modset+seqset+tmset,
            "MMcons+Meff":["MMcons","Meff"],
            "dockQcons+Meff":["dockQcons","Meff"],
            "dockQpair+Meff":["dockQpair","Meff"],
            "MMpair+Meff":["MMpair","Meff"],
            "MMcons+HHprob":["MMcons","HHprob"],
            "dockQcons+HHprob":["dockQcons","HHprob"],
            "dockQpair+HHprob":["dockQpair","HHprob"],
            "MMpair+HHprob":["MMpair","HHprob"],
            "MMcons+longPPVMODEL":["MMcons","longPPVMODEL"],
            "dockQcons+longPPVMODEL":["dockQcons","longPPVMODEL"],
            "dockQpair+longPPVMODEL":["dockQpair","longPPVMODEL"],
            "MMpair+longPPVMODEL":["MMpair","longPPVMODEL"],
            "mets":mets
}
for k in mykeys:
    methodsets[k]=[k]

# Examples
firstsets={"Rosettaset":"N3","PconsDockset":"N3","PconsDockset2":"N3","bigset":"PconsDock","dockingset":"GRAMM","grammset":"GRAMM","pyconsFoldset":"pyconsFold","smallset":"N3","Jackhmmer":"N3","protocolset":"N3","taxaset":"N3","AF2set":"AF2-merged"}
setdata={"Rosettaset":Rosettaset,"AF2set":AF2set,"bigset":bigset,"dockingset":dockingset,"grammset":grammset,"pyconsFoldset":pyconsFoldset,"PconsDockset":PconsDockset,"PconsDockset2":PconsDockset2,"smallset":smallset,"Jackhmmer":oneset,"protocolset":protocolset,"taxaset":taxaset}

cutoff=.23



# Create df with best rosetta predictions
#r=re.compile("^PDB|^pdb|^TMd|GRAMM")
#datasets=df.JHparams.unique()
#notRosettaset=list(filter(r.match, datasets))
#df2=df.loc[~df.JHparams.isin(notRosettaset)].copy()


# We have to think which ones to use..
df2=df.loc[df.JHparams.isin(Rosettaset)].copy()
df2.JHparams="PconsDock"
df2.params="PconsDock"
columns=df2.keys()
df3=pd.DataFrame(columns=columns)
# we need too update the ranks
df2.sort_values("dockQpair",axis=0,inplace=True)
#print(df2.keys())
for code in df2["code"].unique():
    tempdf=df2.loc[df2["code"]==code]
    tempdf["rank"]=tempdf["dockQcons"].rank(method="first",ascending=False)
    df3=pd.concat([df3,tempdf],ignore_index=True)
df=pd.concat([df,df3], ignore_index=True,sort=False)


for i in ["dockQ","MMcons","dockQcons","MMpair","dockQpair","TMscore"]:
    # We have to think which ones to use..
    df2=df.loc[df.JHparams.isin(Rosettaset)].copy()
    df2.JHparams="PconsDock-"+str(i)
    df2.params="PconsDock-"+str(i)
    columns=df2.keys()
    df3=pd.DataFrame(columns=columns)
    # we need too update the ranks
    df2.sort_values(i,axis=0,inplace=True)
    #print(df2.keys())
    for code in df2["code"].unique():
        tempdf=df2.loc[df2["code"]==code]
        tempdf["rank"]=tempdf[i].rank(method="first",ascending=False)
        df3=pd.concat([df3,tempdf],ignore_index=True)
    df=pd.concat([df,df3], ignore_index=True,sort=False)


#sys.exit(0)
# More PconsDock variations
sets={"Rosettaset":Rosettaset,"AF2set":AF2set,"bigset":bigset,"dockingset":dockingset,"pyconsFoldset":pyconsFoldset,"grammset":grammset,"PconsDockset":PconsDockset,"PconsDockset2":PconsDockset2,"Jackhmmer":oneset,"smallset":smallset,"protocolset":protocolset,"taxaset":taxaset}


methods=["dockQpair", "MMpair","dockQcons","MMcons","TMscore"] # "dockQ", 

allrosetta=Rosettaset+["PconsDock"]
for s in sets.keys():
    print (s,sets[s])
    for m in methods:
        # We have to think which ones to use..
        df2=df.loc[df.JHparams.isin(sets[s])].copy()
        df2.JHparams=s+"-"+m
        df2.params=s+"-"+m

        columns=df2.keys()
        df3=pd.DataFrame(columns=columns)

        # we need tpo update the ranks
        df2.sort_values(m,axis=0,inplace=True)
        #print(df2.keys())
        for code in df2["code"].unique():
            tempdf=df2.loc[df2["code"]==code]
            tempdf["rank"]=tempdf[m].rank(method="first",ascending=False)
            df3=pd.concat([df3,tempdf],ignore_index=True)
        df=pd.concat([df,df3], ignore_index=True,sort=False)
        allrosetta+=[s+"-"+m]


df["TMscore"]=df[["tmA","tmB"]].mean(axis=1)
df["MSA"]=df[["msaA","msaB"]].mean(axis=1)
df["TMscore_max"]=df[["tmA","tmB"]].max(axis=1)
df["MSA_max"]=df[["msaA","msaB"]].max(axis=1)
df["TMscore_min"]=df[["tmA","tmB"]].min(axis=1)
df["MSA_min"]=df[["msaA","msaB"]].min(axis=1)
df["numcorrect"]=df["PPV"]*df["contacts"]
df["len"]=df["lenA"]+df["lenB"]



codes=df.code.unique()
df_mean=df.groupby(["JHparams","code"]).mean()
df_max=df.groupby(["JHparams","code"]).max()
df_min=df.groupby(["JHparams","code"]).min()
df_first=df.loc[df["rank"]==1]
#df_max["JHparams"]=df_max["params"]
#df_min["JHparams"]=df_min["params"]
#df_mean["JHparams"]=df_mean["params"]


# Models


# Max/min/mean
datasets=df.JHparams.unique()
print (datasets)
data_mean={}
data_max={}
data_min={}
for d in datasets:
    print (d)
    data_mean[d]=df.loc[df.JHparams==d].groupby(["code"]).mean()
    data_max[d]=df.loc[df.JHparams==d].groupby(["code"]).max()
    data_min[d]=df.loc[df.JHparams==d].groupby(["code"]).min()

#print (data_max["N3-pdb"])
#sys.exit()
data_first={}
data_second={}
for d in datasets:
    data_first[d]=df.loc[ (df["JHparams"]==d)  & (df["rank"]==1)]
    data_second[d]=df.loc[ (df["JHparams"]==d)  & (df["rank"]==2)]


# Extra
    # First we actually do linear regression


    
# Linear regression


reg = LinearRegression()

    
# We need to create a dataset of linear regression 
    # Figures dockQscores- scatter plots
cutoff=0.2
marker=[".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11,".","+","x",4,5,6,7,8,9,10,11]
for s in sets.keys():
    j=0
    printed={}
    print ("scatter",s)
    f, ax = plt.subplots(figsize=(6.5, 6.5))
    selset=setdata[s]
    orgset=firstsets[s]
    for d in selset:
        if d==orgset:continue   
        print (d,orgset)
        print (data_max[orgset])
        print (data_max[d])
        df_merged = pd.merge(data_max[orgset],data_max[d], on=['code'],how="inner")
        plt.scatter(df_merged.dockQ_x,df_merged.dockQ_y,label=d,marker=marker[j])
        for i, txt in enumerate(df_merged.name_x):
            #print (i,txt)
            try: 
                sstr=txt[0:4]
            except:
                sstr=df_merged.name_y[i][0:4]
            if ((df_merged.dockQ_x[i]> cutoff or df_merged.dockQ_y[i]>cutoff) and not (sstr in printed )) :
                ax.annotate(sstr, (df_merged.dockQ_x[i], df_merged.dockQ_y[i]))
                printed[sstr]=1
        j+=1
    x=[0,1.0]
    y=[0,1.0]
    plt.plot(x,y)
    ax.legend()
    ax.set_title("Best model")
    ax.set_xlabel("dockQ - " + orgset)
    ax.set_ylabel("dockQ ")
    
    plt.savefig("scatter-"+s+"-best.png",bbox_inches="tight",dpi=600)
    
    
    
# Figure TM-score vs MSA

    # For max method
sns.set(font_scale=1.4,style="whitegrid",rc={'figure.figsize':(12,12)})
for k in ["dockQ","mm","TMscore"]:
    
     for s in sets.keys():
         print (s)
         selset=setdata[s]
         f, ax = plt.subplots(figsize=(6.5, 6.5))
     
         all_df=df_max.loc[(df_max.params.isin(selset))].reset_index()
         all_df.sort_values("params")
         #print (all_df)
         #plt.scatter(all_df.tmA,all_df.params,color="red")
         ax.set_title("Best model")
         #ax.set_ylabel("Method")
         ax.set_xlabel(k)
         ax.set(xlim=[0,1])
         done={}
         average={}
         length={}
         pos=[]
         POS=[]
         i=0
         data=[]
         keys=[]
         shift=0.2
         for d in sorted(selset,reverse=False):
             #done[d]=0
             #average[d]=all_df.loc[(all_df.params==d)]["tmA"].mean()
             #length[d]=len(all_df.loc[(all_df.params==d)]["tmA"])
             tempdata=all_df.loc[(all_df.params==d)][k].to_list()
             if len(tempdata)>0:
                 pos+=[i]
                 keys+=[d]
                 data+=[tempdata]
                 POS+=[[i]*len(data[i])]
                 print (d,i,len(pos),pos[i],len(data),len(data[i]))
                 i+=1
         ax.set_yticklabels(keys)
         ax.set_yticks(pos)
         ax.tick_params(axis="y",direction="in", left="off",labelleft="on")
         #print (data,POS)
         cutoffA=0.23
         cutoffB=0.49
         cutoffC=0.80
     
         for i in range(len(data)):
             plt.scatter(data[i],POS[i],color="red")
             numgoodA=len([x for x in data[i] if x>cutoffA])
             numgoodB=len([x for x in data[i] if x>cutoffB])
             #sstr=str(numgoodA)+"; "+str(numgoodB)
             sstr=str(numgoodA)
             #ax.annotate(sstr,(.8, i))
             avestr=str(round(np.mean(data[i]),3))
             if k=="dockQ":
                 ax.annotate(avestr+" ("+sstr+")",(.8, i))
             else:
                 ax.annotate(avestr,(.9, i))
             #numgood=all (x > cutoffC for x in data[i])
             #ax.annotate("High:  "+str(numgood),(.8, i))
     
         ax.violinplot(data,pos, points=80, vert=False, widths=0.7,
                           showmeans=True, showextrema=True, showmedians=False)    
     
         plt.savefig("violin-"+k+"-best-"+s+".png",bbox_inches="tight",dpi=600)
     
     
         
     for s in sets.keys():
         selset=setdata[s]
         print (selset)
         tempdf=df.loc[df.params.isin(selset)]
         print (tempdf)
         sns.set(font_scale=1.4,style="whitegrid",rc={'figure.figsize':(12,12)})
         jplot=sns.jointplot(data=tempdf, x="log10 Meff", y=k,kind="scatter",marker="+",s=100 ,hue="params",legend=False) #
         jplot.plot_joint(sns.kdeplot, color="r", zorder=0, levels=6,legend=False)
         #sns_plot=sns.jointplot(data=newdf, x=k, y=l,hue="good",kind="kde")
         jplot.ax_joint.set_xticks([0,1,2,3,4])
         jplot.ax_joint.set_xticklabels([1,10,100,1000,10000])
         jplot.ax_joint.set_xlabel("Meff")
         jplot.savefig("Meff-"+k+"-"+s+".png",bbox_inches="tight",dpi=600)
         plt.close(fig="all")
         #jplot=sns.jointplot(data=tempdf, x="log10 Meff", y="TMscore",hue="params",kind="scatter",marker="+",s=100)
         #jplot.plot_joint(sns.kdeplot, color="r", zorder=0, levels=6,legend=False)
         #sns_plot=sns.jointplot(data=newdf, x=k, y=l,hue="good",kind="kde")
         #jplot.ax_joint.set_xticks([0,1,2,3,4])
         #jplot.ax_joint.set_xticklabels([1,10,100,1000,10000])
         #jplot.ax_joint.set_xlabel("Meff")
         #jplot.savefig("Meff-TM-"+s+".png",bbox_inches="tight",dpi=600)
         #plt.close(fig="all")
     
     
     goodcodes=df.loc[(df[k]>cutoff)]["CODE"].unique()
     
             
     
         
     # First ranked model
     for s in sets.keys():
         #print (s)
         selset=setdata[s]
         f, ax = plt.subplots(figsize=(6.5, 6.5))
     
         all_df=df_first.loc[(df_first.params.isin(selset))].reset_index()
         all_df.sort_values("params")
         #print (all_df)
         #plt.scatter(all_df.tmA,all_df.params,color="red")
         ax.set_title("First ranked model")
         #ax.set_ylabel("Method")
         ax.set_xlabel(k)
         ax.set(xlim=[0,1])
         done={}
         average={}
         length={}
         pos=[]
         POS=[]
         i=0
         data=[]
         keys=[]
         shift=0.2
         for d in sorted(selset,reverse=False):
             #done[d]=0
             #average[d]=all_df.loc[(all_df.params==d)]["tmA"].mean()
             #length[d]=len(all_df.loc[(all_df.params==d)]["tmA"])
             tempdata=all_df.loc[(all_df.params==d)][k].to_list()
             if (len(tempdata)>0):
                 pos+=[i]
                 keys+=[d]
                 data+=[tempdata]
                 POS+=[[i]*len(data[i])]
                 #print (d,i,len(pos),pos[i],len(data),len(data[i]))
                 i+=1
         ax.set_yticklabels(keys)
         ax.set_yticks(pos)
         ax.tick_params(axis="y",direction="in", left="off",labelleft="on")
         #print (data,POS)
         cutoffA=0.23
         cutoffB=0.49
         cutoffC=0.80
     
         for i in range(len(data)):
             plt.scatter(data[i],POS[i],color="red")
             numgoodA=len([x for x in data[i] if x>cutoffA])
             numgoodB=len([x for x in data[i] if x>cutoffB])
             #sstr=str(numgoodA)+"; "+str(numgoodB)
             avestr=str(round(np.mean(data[i]),3))
             #ax.annotate(avestr,(.8, i))
             sstr=str(numgoodA)
             if k=="dockQ":
                 ax.annotate(avestr+" ("+sstr+")",(.8, i))
             else:
                 ax.annotate(avestr,(.9, i))
             #numgood=all (x > cutoffC for x in data[i])
             #ax.annotate("High:  "+str(numgood),(.8, i))
     
         ax.violinplot(data,pos, points=80, vert=False, widths=0.7,
                           showmeans=True, showextrema=True, showmedians=False)    
     
         plt.savefig("violin-"+k+"-first-"+s+".png",bbox_inches="tight",dpi=600)

         dic_t={"taxa":["Mixed","Eukaryota","Bacteria","Archaea","Viruses","+"],
                "log10 Meff-int":["1-10","10-100","100-1000","1000-10000","+10000","+","+","+"],
                "log10 contacts-int":["1-10","10-100","100-1000","1000-10000","10000+","0-1","+","+","+"],
                }
         for t in ["taxa","log10 Meff-int","log10 contacts-int"]:
              tempdf=df_first.loc[(df_first.params.isin(selset))].reset_index()
              f, ax = plt.subplots(figsize=(6.5, 6.5))
              #k="dockQ"
              #plt.scatter(tempdf.dockQ,tempdf.taxa)
              #done[d]=0
              #average[d]=all_df.loc[(all_df.params==d)]["tmA"].mean()
              #length[d]=len(all_df.loc[(all_df.params==d)]["tmA"]
              tempdata=tempdf.loc[(tempdf[t]==d)][k].to_list()
              i=0
              data=[]
              keys=[]
              POS=[]
              pos=[]
              taxas=["Mixed","Eukaryota","Bacteria","Archaea","Viruses"]
              for d in sorted(tempdf[t].unique(),reverse=False):
                  print (t,d)
                  tempdata=tempdf.loc[tempdf[t]==d][k].to_list()
                  if len(tempdata)>0:
                      pos+=[i] 
                      #keys+=[str(d)+"-"+dic_t[t][d]]
                      keys+=[dic_t[t][d]]
                      data+=[tempdata]
                      POS+=[[i]*len(data[i])]
                      i+=1
                      
              ax.set_xticklabels(keys)
              ax.set_xticks(pos)
              ax.set(ylim=[0,1])
              ax.set_title("Performance for different "+t)
              ax.set_xlabel(t)
              ax.set_ylabel(k)
              
              for i in range(len(data)):
                  plt.scatter(POS[i],data[i],color="red")
                  numgoodA=len([x for x in data[i] if x>cutoffA])
                  numgoodB=len([x for x in data[i] if x>cutoffB])
                  #sstr=str(numgoodA)+"; "+str(numgoodB)
                  #sstr=str(numgoodA)
                  #ax.annotate(sstr,(.8, i))
                  avestr=str(round(np.mean(data[i]),3))
                  ax.annotate(avestr,(i,.9))
                  #numgood=all (x > cutoffC for x in data[i])
                  #ax.annotate("High:  "+str(numgood),(.8, i))
     
              #print (data,pos)
              ax.violinplot(data,pos, points=80, vert=True, widths=0.7,
                      showmeans=True, showextrema=True, showmedians=False)    
              plt.savefig("violin-"+k+"-"+t+"-"+s+".png",bbox_inches="tight",dpi=600)
  


# Figures dockQscores- scatter plots
for s in sets.keys():
    j=0
    printed={}
    f, ax = plt.subplots(figsize=(6.5, 6.5))
    selset=setdata[s]
    orgset=firstsets[s]
    for d in selset:
        if d==orgset:continue   
        print (d)
        df_merged = pd.merge(data_first[orgset],data_first[d], on=['code'],how="inner")
        plt.scatter(df_merged.dockQ_x,df_merged.dockQ_y,label=d,marker=marker[j])
        for i, txt in enumerate(df_merged.name_x):
            #print (i,txt)
            try: 
                sstr=txt[0:4]
            except:
                sstr=df_merged.name_y[i][0:4]
            if ((df_merged.dockQ_x[i]> cutoff or df_merged.dockQ_y[i]>cutoff) and not (sstr in printed )) :
                #ax.annotate(sstr, (df_merged.dockQ_x[i], df_merged.dockQ_y[i]))
                printed[sstr]=1
    j+=1
    x=[0,1.0]
    y=[0,1.0]
    plt.plot(x,y)
    ax.legend()
    ax.set_title("First ranked model  ")
    ax.set_xlabel("dockQ - " + orgset)
    ax.set_ylabel("dockQ ")
    
    plt.savefig("scatter-"+s+"-first.png",bbox_inches="tight",dpi=600)
    plt.close(fig="all")


    # Figures dockQscores- scatter plots
cutoff=0.5
printed={}
for s in sets.keys():
    j=0
    print ("scatter",s)
    f, ax = plt.subplots(figsize=(6.5, 6.5))
    selset=setdata[s]
    orgset=firstsets[s]
    for d in selset:
        if d==orgset:continue   
        print (d)
        df_merged = pd.merge(data_max[orgset],data_max[d], on=['code'],how="inner")
        plt.scatter(df_merged.mm_x,df_merged.mm_y,label=d  ,marker=marker[j])
        for i, txt in enumerate(df_merged.name_x):
            #print (i,txt)
            try: 
                sstr=txt[0:4]
            except:
                sstr=df_merged.name_y[i][0:4]
            if ((df_merged.mm_x[i]> cutoff or df_merged.mm_y[i]>cutoff) and not (sstr in printed )) :
                ax.annotate(sstr, (df_merged.mm_x[i], df_merged.mm_y[i]))
                printed[sstr]=1
    j+=1
    x=[0,1.0]
    y=[0,1.0]
    plt.plot(x,y)
    ax.legend()
    ax.set_title("MM scores (best)  ")
    ax.set_xlabel("mm - " + orgset)
    ax.set_ylabel("mm ")
    
    plt.savefig("scatter-mm-"+s+"-best.png",bbox_inches="tight",dpi=600)


# Figures mmscores- scatter plots
printed={}
for s in sets.keys():
    j=0
    f, ax = plt.subplots(figsize=(6.5, 6.5))
    selset=setdata[s]
    orgset=firstsets[s]
    for d in selset:
        if d==orgset:continue   
        print (d)
        df_merged = pd.merge(data_first[orgset],data_first[d], on=['code'],how="inner")
        plt.scatter(df_merged.mm_x,df_merged.mm_y,label=d ,marker=marker[j])
        for i, txt in enumerate(df_merged.name_x):
            #print (i,txt)
            try: 
                sstr=txt[0:4]
            except:
                sstr=df_merged.name_y[i][0:4]
            if ((df_merged.mm_x[i]> cutoff or df_merged.mm_y[i]>cutoff) and not (sstr in printed )) :
                ax.annotate(sstr, (df_merged.mm_x[i], df_merged.mm_y[i]))
                printed[sstr]=1
    j+=1
    x=[0,1.0]
    y=[0,1.0]
    plt.plot(x,y)
    ax.legend()
    ax.set_title("MM scores (first)  ")
    ax.set_xlabel("mm - " + orgset)
    ax.set_ylabel("mm ")
    
    plt.savefig("scatter-mm-"+s+"-first.png",bbox_inches="tight",dpi=600)
    plt.close(fig="all")
    

# Just a fix with a few pconsdock valuse
df.dockQpair=df.dockQpair.apply(lambda x: 0 if x>1 else x)
#df.dockQcons=df.pcs.apply(lambda x: 0 if x<0 else x)
#df.MMcons=df.MMcons.apply(lambda x: 0 if x<0 else x)
    


# Some violin plots for life
cutoff=0.23
corrcut=0.2
goodmodels=list(df.loc[(df.dockQ>cutoff)&(df.params.isin(Rosettaset))].code.unique())
good_df=df.loc[(df.code.isin(goodmodels))&(df.params.isin(Rosettaset))] #.reset_index()
df["good"]=df["code"].isin(goodmodels)
#df["good"]=df.loc[(df.dockQ>cutoff)]
df.loc[(df.dockQ>cutoff),"goodmodels"]=True
df.loc[(df.dockQ<=cutoff),"goodmodels"]=False


df_good = pd.DataFrame({"Method":[]})

r=re.compile("^PDB|^pdb") #|^TMd|GRAMM")
datasets=df.JHparams.unique()
PDBset=list(filter(r.match, datasets))

for s in df.loc[~df.JHparams.isin(PDBset)]["params"].unique():
    codes=df.loc[(df.params.isin([s]))&(df.dockQ>cutoff)]["code"].unique()
    m={"Method":[s]}
    for c in codes:
        dockQ=df.loc[(df.params.isin([s]))&(df.code==c)]["dockQ"].max()
        m[c]=[dockQ]
    df_good=df_good.append(pd.DataFrame.from_dict(m),ignore_index=True)

#print (df_good)
df_good.to_csv("goodmodels.csv")

newdf=df.loc[df.params.isin(Rosettaset)]
# 1d and 2d plots


for s in sets2.keys():
    tempdf=df.loc[(df.params.isin(sets2[s])&(df["rank"]==1))]
    for key in mykeys:
        print ("test1",s,key)
        #print ("test2",len(tempdf.good))
        #print ("test3",len(tempdf[key]))
        f, ax = plt.subplots(figsize=(6.5, 6.5))
        sns.set(font_scale=1.4,style="whitegrid",rc={'figure.figsize':(12,12)})
        sns_plot=sns.boxplot(tempdf.good,tempdf[key])
        sns_plot.set(title="Good vs Bad models: "+key+"-"+s)    
        #violin_plot=sns.violinplot(tempdf.good,tempdf[key], points=80,
        #                           vert=True, widths=0.7, showmeans=True,
        #                           showextrema=True,
        #                           showmedians=False).set_title("Good vs Bad models: "+key+"-"+s)    
        fig=sns_plot.get_figure()
        #vio=violin_plot.get_figure()
        plt.scatter(tempdf.good,tempdf[key],color="red")
        fig.savefig("comparison-"+key+"-"+s+"-dockQ.png",bbox_inches="tight",dpi=600)
        #vio.savefig("comparison-violin-"+key+"-"+s+"-dockQ.png",bbox_inches="tight",dpi=600)
        plt.close(fig="all")

#sys.exit()

for s in sets2.keys():
    tempdf=df.loc[(df.params.isin(sets2[s])&(df["rank"]==1))]
    for key in mykeys:
        print (s,key)
        #codes=df_max.loc[df_max.params.isin(selset)]["CODE"].unique()
        f, ax = plt.subplots(figsize=(6.5, 6.5))
        plt.scatter(tempdf[key],tempdf.dockQ)
        #ax.legend(loc = 'upper left')
        ax.set(ylim=[0,1.0])
        #ax.set(xlim=[0,1.0])
        ax.set_title("Comparison "+key+" vs dockQ in "+s)
        ax.set_ylabel("dockQ (first ranked model)")
        ax.set_xlabel(key)
        
        plt.savefig("scatter-"+key+"-"+s+"-dockQ.png",bbox_inches="tight",dpi=600)
        plt.close(fig="all")
        print (tempdf[[key,"dockQ","params"]])
        # Joint plot crashes sometime - do not know why
        #sns_plot=sns.jointplot(data=tempdf,x=key,y="dockQ",hue="params",kind="hist") # reg crashes
        
        #sns_plot.savefig("joint-"+key+"-"+s+"-dockQ.png",bbox_inches="tight",dpi=600)
        #plt.close(fig="all")
        # MSA should be log



sns.set(font_scale=1.4,style="whitegrid",rc={'figure.figsize':(12,12)})
cutoff=0.23        
for s in sets.keys():
    print (s,sets[s])
        
    goodmodels2=list(df.loc[(df.dockQ>cutoff)&(df.params.isin(setdata[s]))].sort_values(by="code").code.unique())
    good_df=df.loc[(df.code.isin(goodmodels2)&(df.params.isin(setdata[s])))].sort_values(by="code") #.reset_index()
    #df["good"]=df["code"].isin(goodmodels)
    #print (goodmodels)

    f, ax = plt.subplots(figsize=(6.5, 6.5))
    i=0
    for k in setdata[s]:
        print ("Set: ",k)
        tempdf=good_df.loc[(good_df.params==k)].sort_values(by="code")
        plt.scatter(tempdf.code,tempdf.dockQ,label=k,marker=marker[i])# ,marker=i)
        i+=1
    plt.xticks(rotation=90)
    ax.legend()
    ax.set_title("dockQ score for good models")
    ax.set_ylabel("dockQ ")
    ax.set_xlabel("model")
    ax.set(ylim=[0,0.8])
    i=0
    data=[]
    pos=[]
    for m in goodmodels2:
        print (m)
        data+=[good_df.loc[(good_df.code==m)]["dockQ"].to_list()]
        pos+=[i]
        i+=1

    print (len(data),len(pos))    
    ax.violinplot(data,pos, points=80, vert=True, widths=0.7,
                      showmeans=True, showextrema=True,  showmedians=False)
    plt.savefig("good-violin-"+s+".png",bbox_inches="tight",dpi=600)
    plt.close(fig="all")

# GOOD SNS plost
cutoff=0.23        
for d in methods:
    for s in sets.keys():
        print (s,sets[s])
        goodmodels2=list(df.loc[(df.dockQ>cutoff)&(df.params.isin(setdata[s]))].sort_values(by="code").code.unique())
        good_df=df.loc[(df.code.isin(goodmodels2)&(df.params.isin(setdata[s])))].sort_values(by="code") #.reset_index()
        #print(good_df)
        #df["good"]=df["code"].isin(goodmodels2)
        #print (goodmodels2)
        f, ax = plt.subplots(figsize=(9.5, 6.5))
        plt.xticks(rotation=90)
        #plt.show()
        ax.set_title('dockQ score for good models: '+s+" "+d)
        ax.set_xlim(-0.5,1)
        ax.set_xlim(0, 1.5*len(goodmodels2))
        #plt.ylim([0, 1])
        #plt.xlim([0, len(goodmodels2)+10])
        sns.set(font_scale=1.4,style="whitegrid",rc={'figure.figsize':(12,12)})
        sns_plot=sns.scatterplot(data=good_df, x="code", y="dockQ", hue="params", size=d) 
        f=sns_plot.get_figure()
        f.savefig("good-bubble-"+d+"-"+s+"-dockQ.png",bbox_inches="tight",dpi=600)
        plt.close(fig="all")

# ROC curves
    

# calculate the fpr and tpr for all thresholds of the classification
for s in sets.keys():
    print ("set",s)
    f, ax = plt.subplots(figsize=(6.5, 6.5))
    dftmp=df.loc[(df.JHparams.isin(setdata[s]))&(df["rank"]==1)]
    correct=dftmp["dockQ"]>cutoff
    # method I: plt
    for d in methods:
        values=dftmp[d]
        fpr, tpr, threshold = metrics.roc_curve(correct, values)
        roc_auc = metrics.auc(fpr, tpr)
        plt.plot(fpr, tpr, label = d+': AUC = %0.2f' % roc_auc)

    ax.set_title('Receiver Operating Characteristic')
    
    plt.legend(loc = 'lower right')
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.savefig("ROC-pconsdock"+s+".png",bbox_inches="tight",dpi=600)


for s in sets.keys():
    f, ax = plt.subplots(figsize=(6.5, 6.5))
    dftmp=df.loc[(df.JHparams.isin(setdata[s]))&df.dockQcons>0]
    ax.set_title('PconsDock comparison: '+s)
    plt.scatter(dftmp.dockQcons,dftmp.dockQpair)
    #plt.legend(loc = 'lower right')
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('PconsDock reverse')
    plt.xlabel('PconsDock 5 models')
    plt.savefig("scatter-pconsdock-"+s+".png",bbox_inches="tight",dpi=600)




for s in sets.keys():
    f, ax = plt.subplots(figsize=(6.5, 6.5))
    dftmp=df.loc[(df.JHparams.isin(setdata[s]))&df.MMcons>0]
    ax.set_title('MMpair comparison: '+s)
    plt.scatter(dftmp.dockQcons,dftmp.MMpair)
    #plt.legend(loc = 'lower right')
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('MMpair reverse')
    plt.xlabel('MMCONSock 5 models')
    plt.savefig("scatter-MMpair-"+s+".png",bbox_inches="tight",dpi=600)
    
# A classifiers
# Measures with significant separation between good and bad


df_performance = pd.DataFrame(
    {'Method' : [],
    'DataSet' : [],
    'Degree' : [],
    'AUC':[],
    'SUM':[],
    'SUM2':[],
    'CORRECT':[],
    'CORRECT2':[],
    'R':[],
    'R2':[],
     'Z':[],
    'RMSD':[]}
    )

for N in [1,2]:
    for degree in [1]:
        if N==1:
            newdf=df
        elif N==2:
            newdf=df.loc[df.code.isin(goodmodels)]
        fig,ax=plt.subplots(figsize=(6.5, 6.5))
        #for m in ["modelinfo","m+t-info","mets"]:  # methodsets.keys():
        for m in methodsets.keys():
            XA=newdf.loc[newdf.params.isin(RosettasetA)][methodsets[m]]
            YA=newdf.loc[newdf.params.isin(RosettasetA)]["dockQ"]
            XB=newdf.loc[newdf.params.isin(RosettasetB)][methodsets[m]]
            YB=newdf.loc[newdf.params.isin(RosettasetB)]["dockQ"]
            XALLA=df.loc[df.params.isin(RosettasetA)][methodsets[m]]
            YALLA=df.loc[df.params.isin(RosettasetA)]["dockQ"]
            XALLB=df.loc[df.params.isin(RosettasetB)][methodsets[m]]
            YALLB=df.loc[df.params.isin(RosettasetB)]["dockQ"]
            polyregA=make_pipeline(PolynomialFeatures(degree),LinearRegression())
            polyregB=make_pipeline(PolynomialFeatures(degree),LinearRegression())
            polyregA.fit(XA,YA)
            polyregB.fit(XB,YB)
            predA=polyregB.predict(XALLA)
            predB=polyregA.predict(XALLB)
            err=mean_squared_error(predA,YALLA)
            errB=mean_squared_error(predB,YALLB)
            fig2,ax2=plt.subplots(figsize=(6.5, 6.5))
            #ax2.scatter(YA,predA)
            #ax2.scatter(YB,predB)
            correctA=YALLA>cutoff
            correctB=YALLB>cutoff
            correct=pd.concat([correctA,correctB])
            Y=pd.concat([YALLA,YALLB])
            pred=np.hstack((predA,predB))
            
            ax2.scatter(Y,pred)
            #fig2.show()
            fpr, tpr, threshold = metrics.roc_curve(correct, pred)
            roc_auc = metrics.auc(fpr, tpr)
            corr=np.corrcoef(Y,pred)
        
        
            YA2=YA.loc[YA>corrcut]
            XA2=XA.loc[YA>corrcut]
            predA2=polyregB.predict(XA2)
            YB2=YB.loc[YB>corrcut]
            XB2=XB.loc[YB>corrcut]
            predB2=polyregA.predict(XB2)
            Y2=pd.concat([YA2,YB2])
            pred2=np.hstack((predA2,predB2))
    
    
            corr2=np.corrcoef(Y2,pred2)
        
        
            tempdfA=df.loc[df.params.isin(RosettasetA)].copy()
            tempdfA["pred"]=predA
            tempdfB=df.loc[df.params.isin(RosettasetB)].copy()
            tempdfB["pred"]=predB
    
            tempdf=pd.concat([tempdfA,tempdfB])
            #tempdf=newdf.loc[newdf.params.isin(RosettasetA+RosettasetB)].copy()
            bestdf=tempdf.loc[tempdf.params.isin(Rosettaset)].sort_values("pred",ascending=False).groupby(["code"]).first()
            #print (bestdf[["CODE","dockQ","MMcons"]])
            c=sum(bestdf["dockQ"]>cutoff)
            s=sum(bestdf["dockQ"])/len(bestdf)
            topdf=tempdf.loc[tempdf.params.isin(Rosettaset)].sort_values("dockQ",ascending=False).groupby(["code"]).first()
            #print (topdf[["CODE","dockQ","MMcons"]])
            t=sum(topdf["dockQ"]>cutoff)
            S=sum(topdf["dockQ"])/len(topdf)
            #print ("test",sum(correct),corr[0,1],c,t,s,S)
            rmsd=np.sqrt(np.sum((pred-Y)**2))/len(pred)

            av=tempdf["pred"].mean()
            std=tempdf["pred"].std()
            av2=tempdf.loc[tempdf.dockQ>cutoff]["pred"].mean()
            #print (m,degree,N,average,std,av2)
            Z=(av2-av)/std
            
            textstr = '\n'.join((
                r'correct=%d (%d)' % (c,t ),
                r'sum=%.3f (%.3f)' % (s,S ),
                r'R=%.3f ' % (corr[0,1] ),
                r'R Y>c=%.3f ' % (corr2[0,1] ),
                r'RMSD=%.1e ' % (rmsd ),
            ))
            df2 =pd.DataFrame(
                {'Method' : [m],
                 'DataSet' : [N],
                 'Degree' : [degree],
                 'AUC':[roc_auc],
                 'SUM':[s],
                 'SUM2':[S],
                 'CORRECT':[c],
                 'CORRECT2':[t],
                 'R':[corr[0,1]],
                 'R2':[corr2[0,1]],
                 'Z':[Z],
                 'RMSD':[rmsd]}
            )
            df_performance=df_performance.append(df2, ignore_index=True)
        
            ax2.text(0.05, 0.8, textstr, transform=ax.transAxes, fontsize=12,
                verticalalignment='top')
            ax2.set_title('Scatter pred vs real dockQ: '+m)
            ax2.set_xlim([0, 0.7])
            ax2.set_ylim([0, 0.7])
            ax2.plot([0, 1], [0, 1],'r--')
            #ax2.plot([0, 0.23], [1, 0.23],'g--')
            #ax2.plot([0.23, 0], [0.23, 1],'g--')
            ax2.set_ylabel('Predicted dockQ')
            ax2.set_xlabel('dockQ')
            fig2.savefig("scatterpred-degree-"+str(degree)+"-set-"+str(N)+"-"+m+".png",bbox_inches="tight",dpi=600)
            #plt.show()
            ax.plot(fpr, tpr, label = m+': AUC = %0.2f' % roc_auc)
            ax.set_title('Receiver Operating Characteristic: ')
            #ax.legend(loc = 'lower right')
            ax.plot([0, 1], [0, 1],'r--')
            ax.set_xlim([0, 1])
            ax.set_ylim([0, 1])
            ax.set_ylabel('True Positive Rate')
            ax.set_xlabel('False Positive Rate')
        fig.savefig("ROC-degree-"+str(degree)+"-set-"+str(N)+".png",bbox_inches="tight",dpi=600)
        plt.close(fig="all")    


print (df_performance)
df_performance.to_csv("performance.csv")

newdf=df.loc[df.params.isin(Rosettaset)]
# 1d and 2d plots
dic_t={"taxa":["","","Mixed","Eukaryota","Bacteria","Archaea","Viruses",""],
       "log10 Meff-int":["","1","10","100","1000","10000","+10000","",""],
       "log10 contacts-int":["","","0","1-10","10-100","100-1000","1000-10000","+10000",""],
       "log10 Meff":["","1","10","100","1000","10000","100000","",""],
       "log10 contacts":["","","0","1","10","100","1000","10000","100000",""],
                }
for k in mykeys:
    try:
        sns.set(font_scale=1.0,style="whitegrid",rc={'figure.figsize':(12,12)})
        sns_plot=sns.displot(newdf,x=k,hue="good",multiple="layer",kind="kde",common_norm=False,fill=True,legend=False)
        #sns_plot.set(xlim=(-2,5))
        #plt.legend(  fontsize = 12, title="", labels=["Correct","Incorrect"], shadow = True, facecolor = 'white') #loc='upper left',
        if k in dic_t:
            sns_plot.set_xticklabels(dic_t[k],rotation=45)
        sns_plot.set(title='Correct and incorrect targets')
        sns_plot.savefig("displot-good-bad-"+k+".png",bbox_inches="tight",dpi=600)
    except:
        try:
            newdf[k+"R"]=newdf.taxa.apply(lambda x:(x+-0.1+0.2*(np.random.rand())))
            #sns_plot=sns.displot(newdf,x=k+"R",hue="good",multiple="layer",kind="kde",common_norm=False,fill=True)
            sns_plot=sns.displot(newdf,x=k,hue="good",multiple="layer",kind="hist",common_norm=False,fill=True,stat="probability",legend=False)
            #plt.legend(  fontsize = 12, title="", labels=["Correct","Incorrect"], shadow = True, facecolor = 'white') #loc='upper left',
            #sns_plot.set(xlim=(-2,5))
            if k in dic_t:
                sns_plot.set_xticklabels(dic_t[k],rotation=45)
            sns_plot.set(title='Correct and incorrect targets')
            sns_plot.savefig("displot-good-bad-"+k+".png",bbox_inches="tight",dpi=600)
        except:
            continue
        
for k in mykeys:
    try:
        sns_plot=sns.displot(newdf,x=k,hue="goodmodels",multiple="layer",kind="kde",common_norm=False,fill=True,legend=False)
        #plt.legend(  fontsize = 12, title="", labels=["Correct","Incorrect"], shadow = True, facecolor = 'white') #loc='upper left',

        #sns_plot.set(xlim=(-2,5))
        sns.set(font_scale=1.4,style="whitegrid",rc={'figure.figsize':(12,12)})
        if k in dic_t:
            sns_plot.set_xticklabels(dic_t[k],rotation=45)
        sns_plot.set(title='Correct and incorrect models')
        sns_plot.savefig("displot-good-bad-model-"+k+".png",bbox_inches="tight",dpi=600)
    except:
        try:
            newdf[k+"R"]=newdf.taxa.apply(lambda x:(x+-0.1+0.2*(np.random.rand())))
            #sns_plot=sns.displot(newdf,x=k+"R",hue="goodmodels",multiple="layer",kind="kde",common_norm=False,fill=True)
            sns_plot=sns.displot(newdf,x=k,hue="goodmodels",multiple="layer",kind="hist",common_norm=False,fill=True,stat="probability",legend=False)
            #plt.legend(  fontsize = 12, title="", labels=["Correct","Incorrect"], shadow = True, facecolor = 'white') #loc='upper left',
            #sns_plot.set(xlim=(-2,5))
            sns.set(font_scale=1.4,style="whitegrid",rc={'figure.figsize':(12,12)})
            if k in dic_t:
                sns_plot.set_xticklabels(dic_t[k],rotation=45)
            sns_plot.set(title='Correct and incorrect models')
            sns_plot.savefig("displot-good-bad-model-"+k+".png",bbox_inches="tight",dpi=600)
        except:
            continue

# we should normalize so that we have roughly the same amount of good and bad entries
x=len(newdf.loc[newdf.good==True])
y=len(newdf.loc[newdf.good==False])
tempdf=newdf.loc[newdf.good==True].copy()
for i in range(1,int(y/x)):
    #print (i)
    #tempdf["copynumber"]=i
    newdf=pd.concat([newdf,tempdf], ignore_index=True,sort=False)

    
for k in mykeys:
    for l in mykeys:
        if k==l: continue
        try:
            jplot=sns.jointplot(data=newdf, x=k, y=l,hue="good",kind="scatter",marker="+",legend=False)
            jplot.plot_joint(sns.kdeplot, color="r", zorder=0, levels=6,legend=False)
            #sns_plot=sns.jointplot(data=newdf, x=k, y=l,hue="good",kind="kde")
            jplot.savefig("jointplot-good-bad-"+k+"-"+l+".png",bbox_inches="tight",dpi=600)
        except:
            continue
        plt.close(fig="all")

for k in mykeys:
    for l in mykeys:
        if k==l: continue
        try:
            jplot=sns.jointplot(data=newdf, x=k, y=l,hue="goodmodels",kind="scatter",marker="+",legend=False)
            jplot.plot_joint(sns.kdeplot, color="r", zorder=0, levels=6,legend=False)
            #sns_plot=sns.jointplot(data=newdf, x=k, y=l,hue="good",kind="kde")
            jplot.savefig("jointplot-good-models-bad-model-"+k+"-"+l+".png",bbox_inches="tight",dpi=600)
        except:
            continue
        plt.close(fig="all")


newdf=df.loc[df.params.isin(AF2set)]
# 1d and 2d plots
dic_t={"taxa":["","","Mixed","Eukaryota","Bacteria","Archaea","Viruses",""],
       "log10 Meff-int":["","1","10","100","1000","10000","+10000","",""],
       "log10 contacts-int":["","","0","1-10","10-100","100-1000","1000-10000","+10000",""],
       "log10 Meff":["","1","10","100","1000","10000","100000","",""],
       "log10 contacts":["","","0","1","10","100","1000","10000","100000",""],
                }
for k in mykeys:
    try:
        sns_plot=sns.displot(newdf,x=k,hue="good",multiple="layer",kind="kde",common_norm=False,fill=True,legend=False)
        #sns_plot.set(xlim=(-2,5))
        #plt.legend(  fontsize = 12, title="", labels=["Correct","Incorrect"], shadow = True, facecolor = 'white') #loc='upper left',
        if k in dic_t:
            sns_plot.set_xticklabels(dic_t[k],rotation=45)
        sns_plot.set(title='Correct and incorrect targets')
        sns_plot.savefig("AF2-displot-good-bad-"+k+".png",bbox_inches="tight",dpi=600)
    except:
        try:
            newdf[k+"R"]=newdf.taxa.apply(lambda x:(x+-0.1+0.2*(np.random.rand())))
            #sns_plot=sns.displot(newdf,x=k+"R",hue="good",multiple="layer",kind="kde",common_norm=False,fill=True)
            sns_plot=sns.displot(newdf,x=k,hue="good",multiple="layer",kind="hist",common_norm=False,fill=True,stat="probability",legend=False)
            #plt.legend(  fontsize = 12, title="", labels=["Correct","Incorrect"], shadow = True, facecolor = 'white') #loc='upper left',
            #sns_plot.set(xlim=(-2,5))
            if k in dic_t:
                sns_plot.set_xticklabels(dic_t[k],rotation=45)
            sns_plot.set(title='Correct and incorrect targets')
            sns_plot.savefig("AF2-displot-good-bad-"+k+".png",bbox_inches="tight",dpi=600)
        except:
            continue
        
for k in mykeys:
    try:
        sns_plot=sns.displot(newdf,x=k,hue="goodmodels",multiple="layer",kind="kde",common_norm=False,fill=True,legend=False)
        #plt.legend(  fontsize = 12, title="", labels=["Correct","Incorrect"], shadow = True, facecolor = 'white') #loc='upper left',

        #sns_plot.set(xlim=(-2,5))
        sns.set(font_scale=1.4,style="whitegrid",rc={'figure.figsize':(12,12)})
        if k in dic_t:
            sns_plot.set_xticklabels(dic_t[k],rotation=45)
        sns_plot.set(title='Correct and incorrect models')
        sns_plot.savefig("AF2-displot-good-bad-model-"+k+".png",bbox_inches="tight",dpi=600)
    except:
        try:
            newdf[k+"R"]=newdf.taxa.apply(lambda x:(x+-0.1+0.2*(np.random.rand())))
            #sns_plot=sns.displot(newdf,x=k+"R",hue="goodmodels",multiple="layer",kind="kde",common_norm=False,fill=True)
            sns_plot=sns.displot(newdf,x=k,hue="goodmodels",multiple="layer",kind="hist",common_norm=False,fill=True,stat="probability",legend=False)
            #plt.legend(  fontsize = 12, title="", labels=["Correct","Incorrect"], shadow = True, facecolor = 'white') #loc='upper left',
            #sns_plot.set(xlim=(-2,5))
            sns.set(font_scale=1.4,style="whitegrid",rc={'figure.figsize':(12,12)})
            if k in dic_t:
                sns_plot.set_xticklabels(dic_t[k],rotation=45)
            sns_plot.set(title='Correct and incorrect models')
            sns_plot.savefig("AF2-displot-good-bad-model-"+k+".png",bbox_inches="tight",dpi=600)
        except:
            continue

# we should normalize so that we have roughly the same amount of good and bad entries
x=len(newdf.loc[newdf.good==True])
y=len(newdf.loc[newdf.good==False])
tempdf=newdf.loc[newdf.good==True].copy()
for i in range(1,int(y/x)):
    #print (i)
    #tempdf["copynumber"]=i
    newdf=pd.concat([newdf,tempdf], ignore_index=True,sort=False)

    
for k in mykeys:
    for l in mykeys:
        if k==l: continue
        try:
            jplot=sns.jointplot(data=newdf, x=k, y=l,hue="good",kind="scatter",marker="+",legend=False)
            jplot.plot_joint(sns.kdeplot, color="r", zorder=0, levels=6,legend=False)
            #sns_plot=sns.jointplot(data=newdf, x=k, y=l,hue="good",kind="kde")
            jplot.savefig("AF2-jointplot-good-bad-"+k+"-"+l+".png",bbox_inches="tight",dpi=600)
        except:
            continue
        plt.close(fig="all")

for k in mykeys:
    for l in mykeys:
        if k==l: continue
        try:
            jplot=sns.jointplot(data=newdf, x=k, y=l,hue="goodmodels",kind="scatter",marker="+",legend=False)
            jplot.plot_joint(sns.kdeplot, color="r", zorder=0, levels=6,legend=False)
            #sns_plot=sns.jointplot(data=newdf, x=k, y=l,hue="good",kind="kde")
            jplot.savefig("AF2-jointplot-good-models-bad-model-"+k+"-"+l+".png",bbox_inches="tight",dpi=600)
        except:
            continue
        plt.close(fig="all")


# Make a list of all models that "shoudl be good"

def get_numexamples(newdf):
    d["num"]=[len(newdf.code.unique())]
    d["numgood"]=[len(newdf.loc[newdf.dockQ>cutoff]["code"].unique())]
    d["nummod"]=[len(newdf.code)]
    d["numgoodmod"]=[len(newdf.loc[newdf.dockQ>cutoff]["code"])]
    d["frac"]=[d["numgood"][0]/(d["num"][0]+0.0001)]
    d["fracmod"]=[d["numgoodmod"][0]/(d["nummod"][0]+0.0001)]
    return(d)
    

df_filter=pd.DataFrame()
tiny=1.e-20
for k in mykeys:
    dfmax=df.loc[(df.JHparams.isin(Rosettaset))][k].max()+tiny
    dfmin=df.loc[(df.JHparams.isin(Rosettaset))][k].min()-tiny
    dfdiff=(dfmax-dfmin)/3
    for dmin in np.arange(dfmin,dfmax,dfdiff):
        for dmax in np.arange(dmin+dfdiff,dfmax,dfdiff):
            #print (k,dmin,dmax)
            newdf=df.loc[(df.JHparams.isin(Rosettaset)&(df[k]<=dmax)&(df[k]>=dmin))]
            d={k+"_min":[dmin],k+"_max":[dmax]}
            d.update(get_numexamples(newdf))
            df_filter=df_filter.append(pd.DataFrame.from_dict(d),ignore_index=True)


#df_filter=pd.DataFrame()
p=0
P=0
M=0
m=0
h=0
#df_filter=pd.DataFrame()
m=0
M=0
P=0

for c in [0,30,100]:
    for l in [0,1,10]:
        for m in [100,10,0]:
            for MMcons in [0,0.6]:
                for MCC in [0,0.6]:
                    d={"Meff_min":[c],"contacts_min":[l],"med_max":[m],"MMcons_min":[MMcons],"longMCCMODEL_min":[MCC],"TMscore_min":[0]}
                    #print(d)
                    newdf=df.loc[
                        (df.JHparams.isin(Rosettaset))&
                        (df.Meff>=d["Meff_min"][0]) &
                        (df.contacts>=d["contacts_min"][0]) &
                        (df.med<=d["med_max"][0]) &
                        (df.TMscore>=d["TMscore_min"][0]) &
                        (df.MMcons>=d["MMcons_min"][0]) &
                        (df.longMCCMODEL>=d["longMCCMODEL_min"][0])
                        ]
                    d.update(get_numexamples(newdf))
                    df_filter=df_filter.append(pd.DataFrame.from_dict(d),ignore_index=True)
#print (df_filter)                            
#print (df_filter)                            
df_filter.to_csv("fraction.csv")

#outlist=["MMcons_min","Meff_min","contacts_min","med_max","longMCCMODEL_min","num","numgood","nummod","numgoodmod","frac","fracmod"]
#
#finalset=[
#     {"Meff_min":0,"contacts_min":0, "longMCCMODEL_min":0,"MMcons_min":0},
#     {"Meff_min":30,"contacts_min":0,"longMCCMODEL_min":0,"MMcons_min":0},
#     {"Meff_min":100,"contacts_min":0,"longMCCMODEL_min":0,"MMcons_min":0},
#     {"Meff_min":300,"contacts_min":0,"longMCCMODEL_min":0,"MMcons_min":0},
#     {"Meff_min":30,"contacts_min":1,"longMCCMODEL_min":0,"MMcons_min":0},
#     {"Meff_min":30,"contacts_min":5,"longMCCMODEL_min":0,"MMcons_min":0},
#     {"Meff_min":30,"contacts_min":10,"longMCCMODEL_min":0,"MMcons_min":0},
#     {"Meff_min":30,"contacts_min":0,"med_max":10,"MMcons_min":0.6},
#     {"Meff_min":30,"contacts_min":1,"med_max":10,"MMcons_min":0.6},
#     {"Meff_min":30,"contacts_min":10,"med_max":10,"MMcons_min":0.6},
#     {"Meff_min":30,"contacts_min":1,"longMCCMODEL_min":0.5,"MMcons_min":0},
#     {"Meff_min":30,"contacts_min":1,"longMCCMODEL_min":0,"MMcons_min":0.6},
#     {"Meff_min":30,"contacts_min":1,"longMCCMODEL_min":0.5,"MMcons_min":0.6},
#     {"Meff_min":30,"contacts_min":1,"med_max":10,"longMCCMODEL_min":0.5,"MMcons_min":0.6},
#     {"Meff_min":30,"contacts_min":1,"med_max":10,"longMCCMODEL_min":0.5,"dockQcons_min":0.4},
#     {"Meff_min":30,"contacts_min":1,"med_max":10,"longMCCMODEL_min":0.5,"MMpair_min":0.6},
#     {"Meff_min":30,"contacts_min":1,"med_max":10,"longMCCMODEL_min":0.5,"dockQpair_min":0.1},
#     {"Meff_min":30,"contacts_min":1,"med_max":10,"longMCCMODEL_min":0.5,"MMcons_min":0.8,"dockQcons_min":0.4},
#     {"Meff_min":30,"contacts_min":1,"med_max":10,"longMCCMODEL_min":0.,"MMcons_min":0.6},
#     {"Meff_min":30,"contacts_min":1,"med_max":10,"longMCCMODEL_min":0.,"dockQcons_min":0.4},
#     {"Meff_min":30,"contacts_min":1,"med_max":10,"longMCCMODEL_min":0.,"MMpair_min":0.6},
#     {"Meff_min":30,"contacts_min":1,"med_max":10,"longMCCMODEL_min":0.,"dockQpair_min":0.1},
#     {"Meff_min":30,"contacts_min":1,"med_max":10,"longMCCMODEL_min":0.,"MMcons_min":0.8,"dockQcons_min":0.4},
#     {"Meff_min":30,"contacts_min":1,"med_max":0,"longMCCMODEL_min":0.5,"MMcons_min":0.6},
#     {"Meff_min":30,"contacts_min":1,"med_max":0,"longMCCMODEL_min":0.5,"dockQcons_min":0.4},
#     {"Meff_min":30,"contacts_min":1,"med_max":0,"longMCCMODEL_min":0.5,"MMpair_min":0.6},
#     {"Meff_min":30,"contacts_min":1,"med_max":0,"longMCCMODEL_min":0.5,"dockQpair_min":0.1},
#     {"Meff_min":30,"contacts_min":1,"med_max":0,"longMCCMODEL_min":0.5,"MMcons_min":0.8,"dockQcons_min":0.4},
#    ]
#
#df_final=pd.DataFrame()
#for s in finalset:
#    newdf=df.loc[(df.params.isin(Rosettaset))]
#    d=s
#    print (s)
#    for k in s.keys():
#        print (k)
#        if "_max" in k:
#            key=k.replace("_max","")
#            newdf=newdf.loc[newdf[key]<=s[k]]
#        else:
#            key=k.replace("_min","")
#            newdf=newdf.loc[newdf[key]>=s[k]]
#    #print(newdf)
#    d.update(get_numexamples(newdf))
#    df_final=df_final.append(pd.DataFrame.from_dict(d),ignore_index=True)        
##df_final    
#
#
#
#    #newdf=df.loc[(df.JHparams.isin(Rosettaset))&(df.Meff>30)&(df.med<10)&(df.long>1)
##             &(df.MMcons>0.6)&(df.longMCCMODEL>0.5)]
#
#
#
#df_final.to_csv("finalset.csv")


# Make summary csv
summary_df=pd.DataFrame(columns=["params","Num","<dockQ>","<TM>","Num-first","<dockQ-first>","<TM-first>","Num-best","<dockQ-best>","<TM-best>"])
cutoff=0.23
#df_max=df.groupby(["JHparams","code"]).max()
for i in df.params.unique():
    df_all=df.loc[(df.params==i)]
    df_best=df_max.loc[(df_max.params==i)]
    df_first=df.loc[(df.params==i)&(df["rank"]==1)]
    #print (df_first)
    df_temp=pd.DataFrame(
         [[i,len(df_all.loc[df_all.dockQ>cutoff]),np.mean(df_all.dockQ),np.mean(df_all.TMscore),
         len(df_first.loc[df_first.dockQ>cutoff]),np.mean(df_first.dockQ),np.mean(df_first.TMscore),
         len(df_best.loc[df_best.dockQ>cutoff]),np.mean(df_best.dockQ),np.mean(df_best.TMscore)]],
         columns=["params","Num","<dockQ>","<TM>","Num-first","<dockQ-first>","<TM-first>","Num-best","<dockQ-best>","<TM-best>"]
        )
    #print (df_temp)
    #pd.concat([summary_df,df_temp],ignore_index=True)
    summary_df=summary_df.append(df_temp)
    

summary_df.to_csv("summary-sets.csv")
