#!/usr/bin/env python
# coding: utf-8

# In[1]:


import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import re
import pandas as pd


# In[2]:


import sklearn
from sklearn.linear_model import LinearRegression
#rom sklearn.ensemble import RandomForestClassifier
#from sklearn.linear_model import LassoLars

from sklearn.linear_model import TweedieRegressor

from sklearn.linear_model import (
    LinearRegression, TheilSenRegressor, RANSACRegressor, HuberRegressor)
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline


# In[3]:


def get_ranks(good_df,ypred):
    cutoff=0.2
    #print (codedata,ypred,ydata)
    #print (len(ypred),len(good_df.mm.to_list()))
    d = {'code':good_df.code.to_list(),'pred':ypred,
         "dockQ":good_df.dockQ.to_list(),"params":good_df.params.to_list(),
        "mm":good_df.mm.to_list()}
    res_df=pd.DataFrame(d).sort_values("pred")
    #res_df.sort_values("CODE")

    i=0
    sumdiff=0
    mmdiff=0
    correct=0
    for code in res_df.code.unique():
        topranked=res_df.loc[res_df.code==code].sort_values("pred")["dockQ"][-1:].max()
        best=res_df.loc[res_df.code==code]["dockQ"].max()
        mmtop=res_df.loc[res_df.code==code].sort_values("pred")["mm"][-1:].max()
        mmbest=res_df.loc[res_df.code==code]["mm"].max()
        #print (code,topranked,best)
        if best>cutoff:
            i+=1
            sumdiff+=best-topranked
            mmdiff+=mmbest-mmtop
            if topranked>cutoff:
                correct+=1
    #print (i,correct,round(sumdiff/i,3))            
    return (i,correct,sumdiff,mmdiff)


# In[4]:


reg = LinearRegression()
#reg=RandomForestClassifier(n_estimators=10)
#reg=LassoLars(alpha=.1)
#reg = TweedieRegressor(power=1, alpha=0.5, link='log')
#reg=TheilSenRegressor(random_state=42)
#reg=RANSACRegressor(random_state=42)
#reg=HuberRegressor()


# In[5]:


df=pd.read_csv("~/Downloads/summary.csv",sep=",")
df=df.dropna()
#df.JHparams.unique()

# In[5b]




# In[6]:


sns.set(style="whitegrid")


# In[7]:


df["name"]
df.keys()


# In[8]:



df["code"]=df["name"].str[:4]
df["CODE"]=df["code"]
df["params"]=df["JHparams"]
datasets=df.JHparams.unique()
datasets


# In[9]:



r=re.compile("^PDB|^pdb|^TMd|gramm")
datasets=df.JHparams.unique()
#notRosettaset=list(filter(r.match, datasets))
#df2=df.loc[~df.JHparams.isin(notRosettaset)].copy()
Rosettaset=['-N 1 -E 1.e-10 --cpu 8 ','-N 3 --cpu 8', 
            'N1-cov90','N3-cov90', 'N1-N3-multimerged', 'N1-N3-new', 'N1-N3-cov90', 'N1-N3-cov-nocov',
            'rbh-jh',"confold"] 
# We have to think which ones to use..
df2=df.loc[df.JHparams.isin(Rosettaset)].copy()
df2.JHparams="trRosetta"
df2.params="trRosetta"


# In[10]:


df=pd.concat([df,df2], ignore_index=True,sort=False)
df.fillna(0)
#df.JHparams.unique()


# In[11]:


#df.loc[df.JHparams=="N1-N3-new"]
Dockingset=Rosettaset+["gramm","TMdock-double"]


# In[12]:


r=re.compile("^PDB|^pdb")
datasets=df.JHparams.unique()
pdbset=list(filter(r.match, datasets))
df2=df.loc[(df.JHparams.isin(pdbset))&(~df.JHparams.isin(["pdbconfold"]))].copy()
df2.JHparams="PDB"
df2.params="PDB"
df2


# In[13]:


df=pd.concat([df,df2], ignore_index=True,sort=False)
df.fillna(0)
df.params.unique()


# In[14]:


# Add a linear combination of trRosetta
# Does not work
r=re.compile(".*-N.*") # .*cpu.*
datasets=Rosettaset
#JHsets=list(filter(r.match, datasets))


# In[15]:


cutoff=0.2
goodmodels=df.loc[(df.dockQ>cutoff)&(df.JHparams.isin(Rosettaset))].code.unique()
good_df=df.loc[(df.code.isin(goodmodels))&(df.JHparams.isin(Rosettaset))] #.reset_index()
#df.dockQ.dtypes


# In[16]:


ydata=good_df.dockQ

#cols=["lenA","lenB","msa","long","med","longA","medA","longB","medB","tmA","tmB"]
#xdata=good_df[["lenA","lenB","long","med"]]
#cols=["longA","longB","medA","medB","tmA","tmB"]
cols=["med","long","msa"]
#cols=["msa"]
xdata=good_df[cols]
reg.fit(xdata,ydata)
ypred = reg.predict(xdata)
#f, ax = plt.subplots(figsize=(6.5, 6.5))
#plt.scatter(ydata,ypred)
cc=np.corrcoef(ydata,ypred)
#ax.set_xlabel("dockQ")
#ax.set_ylabel("predicted dockQ")
(i,j,dockq,mm)=get_ranks(good_df,ypred)
#print(i,j,k/i)
#ax.set_title("CC" + " " + str(round(cc[0,1],3))
#            + " Good: " + str(round(j/i,3)) + " Loss: " + str(round(dockq/i,3))
#             + " , " + str(round(mm/i,3))
#             + " Test: " + str(i)
#            )


# In[17]:


xdata=df.loc[df.JHparams.isin(Rosettaset)][cols]
ypred = reg.predict(xdata)
df2=df.loc[df.JHparams.isin(Rosettaset)].reset_index()
df2.loc[:,'pred'] = ypred
topranked=pd.DataFrame(columns = df2.columns) 


# In[18]:



for name in df2.name.unique():
    foo=df2.loc[df2.name==name].sort_values("pred")[-1:].max()
    #print(foo)
    foo_df=pd.DataFrame(foo).transpose()
    #print (foo_df)
    topranked=pd.concat([topranked,foo_df], ignore_index=True)
topranked["JHparams"]="JHranked"
topranked.params="JHranked"
topranked=topranked.drop(["pred","index"],axis=1)
topranked.columns


# In[19]:


#topranked


# In[20]:


df=pd.concat([df,topranked], ignore_index=True,sort=False)
df.fillna(0)
df.params.unique()


# In[21]:


df


# In[22]:


#df["rank"].str.isnumeric().unique()
#df.dtypes


# In[24]:


# We need to check that everything is numeric
#cols=['name', 'JHparams', 'seqid',      'rank']

#for d in df.columns.values.tolist():
#for d in ["rank","len"]:
##    if d=="name": continue
#    if d=="JHparams": continue
#    print (d)
#    print (df.loc[(df[d].str.isnumeric()!=True)][cols+[d]])
#    #print (df.loc[(df[d].str.isnumeric()][d].unique()
#    print (df[d].str.isnumeric().unique())


# In[25]:


# Divide data into datasets


df["tmAB"]=df[["tmA","tmB"]].mean(axis=1)
df["msaAB"]=df[["msaA","msaB"]].mean(axis=1)
df["tmAB_max"]=df[["tmA","tmB"]].max(axis=1)
df["msaAB_max"]=df[["msaA","msaB"]].max(axis=1)
df["tmAB_min"]=df[["tmA","tmB"]].min(axis=1)
df["msaAB_min"]=df[["msaA","msaB"]].min(axis=1)
df["numcorrect"]=df["longPPV"]*df["long"]
df["len"]=df["lenA"]+df["lenB"]

codes=df.code.unique()
df_mean=df.groupby(["JHparams","code"]).mean()
df_max=df.groupby(["JHparams","code"]).max()
df_min=df.groupby(["JHparams","code"]).min()
df_first=df.loc[df["rank"]==1]


# In[26]:


#df.columns.values.tolist()
#df["rank"]==1


# In[27]:


cols=['name', 'JHparams', 'seqid',      'rank']
      
#d="TMdock-nohomology"
#df.loc[df.JHparams==d].groupby(["code"])[cols].first()

#df.loc[(df.JHparams==d) & ((df["rank"].str.isnumeric()!=True))][cols]
#df.loc[(df.JHparams==d)]["rank"].str.isnumeric()
#df.loc[(df.JHparams==d)] 
      
      


# In[28]:


datasets=df.JHparams.unique()
data_mean={}
data_max={}
data_min={}
for d in datasets:
    print (d)
    data_mean[d]=df.loc[df.JHparams==d].groupby(["code"]).mean()
    data_max[d]=df.loc[df.JHparams==d].groupby(["code"]).max()
    data_min[d]=df.loc[df.JHparams==d].groupby(["code"]).min()


# In[29]:


#df.loc[ (df.JHparams=="trRosetta")  & (df["rank"]==1)]


# In[30]:


#df.JHparams.unique()
datasets


# In[31]:


data_first={}
data_second={}
for d in datasets:
    data_first[d]=df.loc[ (df["JHparams"]==d)  & (df["rank"]==1)]
    data_second[d]=df.loc[ (df["JHparams"]==d)  & (df["rank"]==2)]
data_first["trRosetta"]


# In[32]:


#datasets
#d


# In[33]:


df_test= data_first["N1-cov90"]
#data_first["N1-merged"].tmA
df_test["tmB"]


# In[34]:


df_merged = pd.merge(data_first["trRosetta"],data_first[d], on=['code'],how="inner")
#plt.scatter(data_first["trRosetta"].dockQ,data_first["trRosetta"].mm)


# In[37]:


# FInd best method for each model.
selset=Rosettaset
#codes=df_max.loc[df_max.params.isin(selset)]["CODE"].unique()
f, ax = plt.subplots(figsize=(6.5, 6.5))

all_df=df_first.loc[(df_first.params.isin(selset))].reset_index()
all_df.sort_values("JHparams")
#print (all_df)
#plt.scatter(all_df.tmA,all_df.JHparams,color="red")
ax.set_title("First ranked models")
#ax.set_ylabel("Method")
ax.set_xlabel("TM-score")
ax.set(xlim=[0,1.1])
done={}
average=[]
length={}
pos=[]
posA=[]
posB=[]
POS=[]
i=0
dataA=[]
dataB=[]
keys=[]
shift=0.2
for d in sorted(selset,reverse=False):
    #done[d]=0
    average+=[0.5*(all_df.loc[(all_df.params==d)]["tmA"].mean()+all_df.loc[(all_df.params==d)]["tmB"].mean())]
    #length[d]=len(all_df.loc[(all_df.params==d)]["tmA"])
    pos+=[i]
    posA+=[i-shift]
    posB+=[i+shift]
    keys+=[d]
    dataA+=[all_df.loc[(all_df.JHparams==d)]["tmA"].to_list()]
    dataB+=[all_df.loc[(all_df.JHparams==d)]["tmB"].to_list()]
    POS+=[[i]*len(dataA[i])]
    #print (d,i,len(pos),pos[i],len(data),len(data[i]))
    i+=1
ax.set_yticklabels(keys)
ax.set_yticks(pos)
for i in range(len(dataA)):
    #plt.scatter(dataA[i],POS[i],color="red")
    #plt.scatter(dataB[i],POS[i],color="red")
    sstr=str(round(average[i],2))
    ax.annotate(sstr,(1.05, i))
    #numgood=all (x > cutoffC for x in data[i])
    #ax.annotate("High:  "+str(numgood),(.8, i))

ax.violinplot(dataA,posA, points=80, vert=False, widths=0.7,
                      showmeans=True, showextrema=True, showmedians=False)    
ax.violinplot(dataB,posB, points=80, vert=False, widths=0.7,
                      showmeans=True, showextrema=True, showmedians=False)    
plt.savefig("violin-TM.png")


# In[38]:


# FInd best method for each model.
selset=Dockingset
#codes=df_max.loc[df_max.params.isin(selset)]["CODE"].unique()
f, ax = plt.subplots(figsize=(6.5, 6.5))

all_df=df_first.loc[(df_first.params.isin(selset))].reset_index()
all_df.sort_values("JHparams")
#print (all_df)
#plt.scatter(all_df.tmA,all_df.JHparams,color="red")
ax.set_title("First ranked models")
#ax.set_ylabel("Method")
ax.set_xlabel("MM-score")
ax.set(xlim=[0,1.1])
done={}
average=[]
length={}
pos=[]
POS=[]
i=0
data=[]
keys=[]
shift=0.2
for d in sorted(selset,reverse=False):
    #done[d]=0
    average+=[all_df.loc[(all_df.params==d)]["mm"].mean()]
    #length[d]=len(all_df.loc[(all_df.params==d)]["tmA"])
    pos+=[i]
    keys+=[d]
    data+=[all_df.loc[(all_df.JHparams==d)]["mm"].to_list()]
    POS+=[[i]*len(data[i])]
    #print (d,i,len(pos),pos[i],len(data),len(data[i]))
    i+=1
ax.set_yticklabels(keys)
ax.set_yticks(pos)
#print (data,POS)
cutoff=0.5
#cutoffA=0.5
#cutoffB=0.49
#cutoffC=0.80

for i in range(len(data)):
    plt.scatter(data[i],POS[i],color="red")
    numgood=len([x for x in data[i] if x>cutoff])
    sstr=str(round(average[i],2))
    ax.annotate(sstr,(1., i))
    #numgood=all (x > cutoffC for x in data[i])
    #ax.annotate("High:  "+str(numgood),(.8, i))

ax.violinplot(data,pos, points=80, vert=False, widths=0.7,
                      showmeans=True, showextrema=True, showmedians=False)    

plt.savefig("violin-MM.png")


# In[39]:


selset=["N1-N3-cov-nocov","trRosetta","gramm","TMdock-double"]
#codes=df_max.loc[df_max.params.isin(selset)]["CODE"].unique()
f, ax = plt.subplots(figsize=(6.5, 6.5))

all_df=df_max.loc[(df_max.params.isin(selset))].reset_index()
all_df.sort_values("JHparams")
#print (all_df)
#plt.scatter(all_df.tmA,all_df.JHparams,color="red")
ax.set_title("Best model")
#ax.set_ylabel("Method")
ax.set_xlabel("DockQ-score")
ax.set(xlim=[0,1])
done={}
average={}
length={}
pos=[]
POS=[]
i=0
data=[]
keys=[]
shift=0.2
for d in sorted(selset,reverse=False):
    #done[d]=0
    #average[d]=all_df.loc[(all_df.params==d)]["tmA"].mean()
    #length[d]=len(all_df.loc[(all_df.params==d)]["tmA"])
    pos+=[i]
    keys+=[d]
    data+=[all_df.loc[(all_df.JHparams==d)]["dockQ"].to_list()]
    POS+=[[i]*len(data[i])]
    #print (d,i,len(pos),pos[i],len(data),len(data[i]))
    i+=1
ax.set_yticklabels(keys)
ax.set_yticks(pos)
#print (data,POS)
cutoff=0.23
cutoffA=0.23
cutoffB=0.49
cutoffC=0.80

for i in range(len(data)):
    plt.scatter(data[i],POS[i],color="red")
    numgoodA=len([x for x in data[i] if x>cutoffA])
    numgoodB=len([x for x in data[i] if x>cutoffB])
    sstr=str(numgoodA)+"; "+str(numgoodB)
    ax.annotate(sstr,(.9, i))
    #numgood=all (x > cutoffC for x in data[i])
    #ax.annotate("High:  "+str(numgood),(.8, i))

ax.violinplot(data,pos, points=80, vert=False, widths=0.7,
                      showmeans=True, showextrema=True, showmedians=False)    

plt.savefig("violin-dockQ-best.png")


# In[54]:


# FInd best method for each model.
selset=Dockingset
#codes=df_max.loc[df_max.params.isin(selset)]["CODE"].unique()
f, ax = plt.subplots(figsize=(6.5, 6.5))

all_df=df_first.loc[(df_first.params.isin(selset))].reset_index()
all_df.sort_values("JHparams")
#print (all_df)
#plt.scatter(all_df.tmA,all_df.JHparams,color="red")
ax.set_title("First ranked models")
#ax.set_ylabel("Method")
ax.set_xlabel("DockQ-score")
ax.set(xlim=[0,1])
done={}
average={}
length={}
pos=[]
POS=[]
i=0
data=[]
keys=[]
shift=0.2
for d in sorted(selset,reverse=False):
    #done[d]=0
    #average[d]=all_df.loc[(all_df.params==d)]["tmA"].mean()
    #length[d]=len(all_df.loc[(all_df.params==d)]["tmA"])
    pos+=[i]
    keys+=[d]
    data+=[all_df.loc[(all_df.JHparams==d)]["dockQ"].to_list()]
    POS+=[[i]*len(data[i])]
    #print (d,i,len(pos),pos[i],len(data),len(data[i]))
    i+=1
ax.set_yticklabels(keys)
ax.set_yticks(pos)
#print (data,POS)
cutoff=0.23
cutoffA=0.23
cutoffB=0.49
cutoffC=0.80

for i in range(len(data)):
    plt.scatter(data[i],POS[i],color="red")
    numgoodA=len([x for x in data[i] if x>cutoffA])
    numgoodB=len([x for x in data[i] if x>cutoffB])
    length=len(data[i])
    sstr=str(numgoodA)+"; "+str(numgoodB)+"("+str(length)+")"
    ax.annotate(sstr,(.9, i))
    #numgood=all (x > cutoffC for x in data[i])
    #ax.annotate("High:  "+str(numgood),(.8, i))

ax.violinplot(data,pos, points=80, vert=False, widths=0.7,
                      showmeans=True, showextrema=True, showmedians=False)    

plt.savefig("violin-dockQ.png")


# This is the first plot to save
#df_test= data_first["N1-merged"]
#plt.scatter(df_test.tmB,df_test.tmA)
#sns_plot = sns.jointplot(data=df_test, x="tmA", y="tmB")
#sns_plot.savefig("tmA-tmB.png")







# In[41]:


#df_test.loc[(df_test.tmA>0.8 )&(df_test.lenA>300)]["name"]
cutoff=0.2
f, ax = plt.subplots(figsize=(6.5, 6.5))
df_merged = pd.merge(data_max["PDB"],data_max["trRosetta"], on=['code'],how="inner")
plt.scatter(df_merged.dockQ_x,df_merged.dockQ_y,label=d)
ax.set_title("dockQ scores best model ")
ax.set_xlabel("dockQ PDB ")
ax.set_ylabel("dockQ trRosetta")
plt.savefig("trRosetta-PDB-dockQ.png")


# In[42]:


cutoff=0.2
f, ax = plt.subplots(figsize=(6.5, 6.5))
df_merged = pd.merge(data_max["PDB"],data_max["trRosetta"], on=['code'],how="inner")
plt.scatter(df_merged.tmAB_x,df_merged.tmAB_y,label=d)
ax.set_title("TM scores best model ")
ax.set_xlabel("TM PDB ")
ax.set_ylabel("TM trRosetta")
plt.savefig("trRosetta-PDB-TMscore.png")


# In[44]:


data_max.keys()


# In[45]:


cutoff=0.23
f, ax = plt.subplots(figsize=(6.5, 6.5))
for d in ["trRosetta","TMdock-double"]:
    #if d!="N1-merged":continue   
    #print (d)
    df_merged = pd.merge(data_max["gramm"],data_max[d], on=['code'],how="inner")
    #print (df_merged)
    #newdf=df_merged["dockQ_x","dockQ_y"]  #.loc[(df_merged.dockQ_x>cutoff or df_merged.dockQ_y>cutoff) ]
    plt.scatter(df_merged.dockQ_x,df_merged.dockQ_y,label=d)
    for i, txt in enumerate(df_merged.name_x):
        #print (i,txt)
        try: 
            sstr=txt[0:4]
        except:
            sstr=df_merged.name_y[i][0:4]
        if (df_merged.dockQ_x[i]> cutoff or df_merged.dockQ_y[i]>cutoff):
            ax.annotate(sstr, (df_merged.dockQ_x[i], df_merged.dockQ_y[i]))
x=[0,1.0]
y=[0,1.0]
plt.plot(x,y)
ax.legend()
ax.set_title("DockQ scores (best)  ")
ax.set_xlabel("dockQ - gramm")
ax.set_ylabel("dockQ ")

plt.savefig("tr-tm-gramm-best.png")


# In[46]:


cutoff=0.23
f, ax = plt.subplots(figsize=(6.5, 6.5))
baseset="N1-N3-cov-nocov"
for d in Rosettaset:
    if d==baseset:continue   
    #print (d)
    df_merged = pd.merge(data_first[baseset],data_first[d], on=['code'],how="inner")
    #print (df_merged)
    #newdf=df_merged["dockQ_x","dockQ_y"]  #.loc[(df_merged.dockQ_x>cutoff or df_merged.dockQ_y>cutoff) ]
    plt.scatter(df_merged.dockQ_x,df_merged.dockQ_y,label=d)
    for i, txt in enumerate(df_merged.name_x):
        #print (i,txt)
        try: 
            sstr=txt[0:4]
        except:
            sstr=df_merged.name_y[i][0:4]
        #if (df_merged.dockQ_x[i]> cutoff or df_merged.dockQ_y[i]>cutoff):
        if (df_merged.dockQ_y[i]> cutoff and ( df_merged.dockQ_y[i]-0.1>df_merged.dockQ_x[i])):
            ax.annotate(sstr, (df_merged.dockQ_x[i], df_merged.dockQ_y[i]))
x=[0,.8]
y=[0,0.8]
plt.plot(x,y)
ax.legend()
ax.set_title("DockQ scores (best)  ")
ax.set_xlabel("dockQ - "+baseset)
ax.set_ylabel("dockQ ")

plt.savefig("Trrosetta-first.png")


# In[47]:


cutoff=0.23
f, ax = plt.subplots(figsize=(6.5, 6.5))
for d in ["N1-N3-cov-nocov","TMdock-double"]:
    #if d!="N1-merged":continue   
    #print (d)
    df_merged = pd.merge(data_first["gramm"],data_first[d], on=['code'],how="inner")
    #print (df_merged)
    #newdf=df_merged["dockQ_x","dockQ_y"]  #.loc[(df_merged.dockQ_x>cutoff or df_merged.dockQ_y>cutoff) ]
    plt.scatter(df_merged.dockQ_x,df_merged.dockQ_y,label=d)
    for i, txt in enumerate(df_merged.name_x):
        #print (i,txt)
        try: 
            sstr=txt[0:4]
        except:
            sstr=df_merged.name_y[i][0:4]
        if (df_merged.dockQ_x[i]> cutoff or df_merged.dockQ_y[i]>cutoff):
            ax.annotate(sstr, (df_merged.dockQ_x[i], df_merged.dockQ_y[i]))
x=[0,1.0]
y=[0,1.0]
plt.plot(x,y)
ax.legend()
ax.set_title("DockQ scores (first)  ")
ax.set_xlabel("dockQ - gramm")
ax.set_ylabel("dockQ ")

plt.savefig("tr-tm-gramm-first.png")
#cat.add_categories([1])
#df_merged.fillna(0, inplace =True)


# In[48]:


selset=['-N 3 --cpu 8', '-N 1 -E 1.e-100 --cpu 8', '-N 1 -E 1.e-2 ',  '-N 1 -E 1.e-10 --cpu 8 ',  '-N 3 --incE 1.e-2', '-N 5 --cpu 8']
#codes=df_max.loc[df_max.params.isin(selset)]["CODE"].unique()
f, ax = plt.subplots(figsize=(6.5, 6.5))
tempdf=df.loc[(df.JHparams.isin(selset))]
plt.scatter(tempdf.msa,tempdf.dockQ)
plt.savefig("msa-dockq-scatter.png")
sns_plot=sns.jointplot(data=tempdf,x="msa",y="dockQ")
sns_plot.savefig("msa-dockq.png")


# In[49]:


tempdf=df.loc[(df.JHparams.isin(selset))]
plt.scatter(tempdf.msa,tempdf.tmA)
#print (tempdf["msaA"])
#tempdf["logmsaA"]=np.log10(tempdf["msa"]+0.0000001)
plt.savefig("msa-tm-scatter.png")
sns_plot=sns.jointplot(data=tempdf,x="msa",y="tmA")
sns_plot.savefig("msa-tm.png")


# In[52]:


cutoff=1.0
f, ax = plt.subplots(figsize=(6.5, 6.5))
r=re.compile("^PDB|^pdb")
pdbset=list(filter(r.match, datasets))
pdbset=["pdbconfold"]

for d in pdbset:
    #if d!="N1-merged":continue   
    #print (d)
    if d=="pdbcontacts-new":continue
    df_merged = pd.merge(data_max["pdbcontacts-new"],data_max[d], on=['code'],how="inner")
    #print (df_merged)
    #newdf=df_merged["dockQ_x","dockQ_y"]  #.loc[(df_merged.dockQ_x>cutoff or df_merged.dockQ_y>cutoff) ]
    cc=np.corrcoef(df_merged.dockQ_x,df_merged.dockQ_y)
    #print (cc)
    
    
    plt.scatter(df_merged.dockQ_x,df_merged.dockQ_y,label=d)
    for i, txt in enumerate(df_merged.name_x):
        #print (i,txt)
        try: 
            sstr=txt[0:4]
        except:
            sstr=df_merged.name_y[i][0:4]
        if (df_merged.dockQ_x[i]> cutoff or df_merged.dockQ_y[i]>cutoff):
            ax.annotate(sstr, (df_merged.dockQ_x[i], df_merged.dockQ_y[i]))
x=[0,0.9]
y=[0,0.9]
plt.plot(x,y)
ax.legend()
ax.set_title("dockQ scores (frst model), CC=" + str(round(cc[0,1],3)) )
ax.set_xlabel("dockQ ")
ax.set_ylabel("dockQ  "+d)
plt.savefig("pdbconfold.png")
#cat.add_categories([1])
#df_merged.fillna(0, inplace =True)

