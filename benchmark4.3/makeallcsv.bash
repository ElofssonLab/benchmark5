#!/bin/bash -x

p=`pwd`

for i in ./Snakefile  N*/Snakefile rbh*/Snakefile 
do
    j=`dirname $i`
    cd $j
    for k in pymodel/*A.pdb
    do
	l=`basename $k .pdb`
	snakemake -p dimer/$l.taxa
	snakemake -p pymodel/$l.csv
    done
    cd $p
done
for i in  ./Snakefile gramm*/Snakefile tmdock*/Snakefile *confold*/Snakefile
do
    j=`dirname $i`
    cd $j
    for k in pymodel/*.pdb
    do
	l=`basename $k .pdb`
	snakemake -p dimer/$l.taxa
	#rm pymodel/$l.csv
	snakemake -p pymodel/$l.csv
    done
    cd $p
done
#rm results/summary.csv
#snakemake -p results/summary.csv
