#!/bin/bash 

#cd ~/git/benchmark5/
p=`pwd`

#rsync -arv --exclude config.yaml --exclude ".snakemake*" aeserv19a:~/git/benchmark5/ecoli/ ~/git/benchmark5/ecoli/ 
#rsync -arv --exclude config.yaml --exclude ".snakemake*" ~/git/benchmark5/ecoli/  aeserv19a:~/git/benchmark5/ecoli/ 
rsync -arv --exclude config.yaml --exclude ".snakemake*" ~/git/benchmark5/benchmark4.3/N1b-allproteomes/pymodel/*pdb  aeserv19a:~/git/benchmark5/benchmark4.3/N1b-allproteomes/pymodel/ 
rsync -arv --exclude config.yaml --exclude ".snakemake*" --exclude "config.yaml" aeserv19a:~/git/benchmark5/benchmark4.3/N1b-allproteomes/  ~/git/benchmark5/benchmark4.3/N1b-allproteomes/  
rsync -arv --exclude config.yaml --exclude ".snakemake*" ~/git/benchmark5/benchmark4.3/N3-allproteomes/pymodel/*pdb  aeserv19a:~/git/benchmark5/benchmark4.3/N3-allproteomes/pymodel/ 
rsync -arv --exclude config.yaml --exclude ".snakemake*" --exclude "config.yaml" aeserv19a:~/git/benchmark5/benchmark4.3/N3-allproteomes/  ~/git/benchmark5/benchmark4.3/N3-allproteomes/
#rsync -arv --exclude config.yaml --exclude ".snakemake*" ae18b.dyn:~/git/benchmark5/ecoli/ ~/git/benchmark5/ecoli/ 
#rsync -arv --exclude config.yaml --exclude ".snakemake*" ~/git/benchmark5/ecoli/ ae18b.dyn:~/git/benchmark5/ecoli/ 

# for k in ./ N1/ N5/ N1-minprob25/ N3-minprob25/ N1-cov90/ N3-cov90/ N1-cov50/ N3-cov50/ rbh/ rbh-jh/  N1-cov90-minprob25/  N3-cov90-minprob25/ N3-pdb/  N3-merged/   N3-bacterial/ N1b-allproteomes/  N3-allproteomes/
# do
#     rsync -arv aeserv19a:/$p/$k/pymodel/ $p/$k/pymodel/ --exclude ".snakemake*"
# done

# #cd $p/../ecoli/
# #ls dimer/*.trimmed | sed s/.trimmed/.pdb/g | sed s/dimer/pymodel/g > tempfiles.txt
# #for i in `cat tempfiles.txt` ; do ls $i >/dev/null ; done 2>bar
# #gawk '{print $4}' bar | sed s/[\':]//g > pdbfiles.txt
# #while I= read -r i ; do snakemake --cores 3  -p ${i} ; done < pdbfiles.txt &
# 
# 
# 
for k in   N3-allproteomes/   N1b-allproteomes/   # ./ N1/ N5/ N1-minprob25/ N3-minprob25/ N1-cov90/ N3-cov90/ N1-cov50/ N3-cov50/ rbh/ rbh-jh/ N1-cov90-minprob25/  N3-cov90-minprob25/ N3-pdb/ N3-merged/   N3-allproteomes/ 
do
    cd $p/$k
#     ls pymodel/*05.pdb | sed s/_05.pdb/.csv/g > tempcsvfiles.txt
    #../makeallallcsv.bash
    ../makeallallcsv.bash
    cd $p
done
# for k in *confold*/ gramm*/ tmdoc*/ pdbcontacts-new/ raptorx/   N3-bacterial/ N1b-allproteomes/
# do
#     cd $p/$k
#     ls pymodel/*.pdb | sed s/.pdb/.csv/g > tempcsvfiles.txt
#     #gawk '{print "pymodel/"$1"_model1.csv"}' all.txt > tempcsvfiles.txt
#     #gawk '{print "pymodel/"$1"_model2.csv"}' all.txt >> tempcsvfiles.txt
#     #gawk '{print "pymodel/"$1"_model3.csv"}' all.txt >> tempcsvfiles.txt
#     #gawk '{print "pymodel/"$1"_model4.csv"}' all.txt >> tempcsvfiles.txt
#     #gawk '{print "pymodel/"$1"_model5.csv"}' all.txt >> tempcsvfiles.txt
#     for i in `cat tempcsvfiles.txt` ; do ls $i >/dev/null ; done 2>bar
#     gawk '{print $4}' bar | sed s/[\':]//g > csvfiles.txt
#     while I= read -r i ; do snakemake --cores 1  -p ${i} ; done < csvfiles.txt &
#     cd $p
# done
